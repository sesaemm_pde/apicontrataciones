<h1>Secretaría Ejecutiva del Sistema Estatal Anticorrupción</h1>
<h2>Dirección General de Servicios Tecnológicos y Plataforma Digital</h2>
<h3>API-CONTRATACIONES</h3>
<h1>Tecnologías</h1>
<ul>
<li>Java Development Kit 1.8.172</li>
<li>Apache Tomcat 9.0.19</li>
<li>MongoDB Enterprise 4.4</li>
<li>Apache Maven 3.8.1</li>
<h1>Manuales</h1>
<a href="https://gitlab.com/sesaemm_pde/apicontrataciones/-/blob/main/Documentacion/M_I_API_S6_V1.pdf">Manual de instalación</a>
<br>
<a href="https://gitlab.com/sesaemm_pde/apicontrataciones/-/blob/main/Documentacion/BD_API_S6_V1.pdf">Manual de BD</a>
<br>
</ul>