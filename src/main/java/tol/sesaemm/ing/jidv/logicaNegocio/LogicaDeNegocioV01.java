/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaNegocio;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesDocumentoBuyer;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesDocumentoMetadata;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesDocumentoPlanning;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesDocumentoPublisher;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesDocumentoTender;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesListaDocumentoAwards;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesListaDocumentoContracts;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesListaDocumentoParties;
import tol.sesaemm.funciones.s6contrataciones.ContratacionesListaDocumentoTag;
import tol.sesaemm.ing.jidv.integracion.DAOWs;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContratacion;
import tol.sesaemm.javabeans.s6contrataciones.Contrataciones;
import tol.sesaemm.javabeans.s6contrataciones.FiltrosContratacionesWS;

/**
 *
 * @author Ismael Ortz ismael.ortiz@sesaemm.org.mx
 */
public class LogicaDeNegocioV01 implements LogicaDeNegocio
{

    /**
     * Obtener valores por defecto de la estructura post
     *
     * @param estructuraPost Estructura para la peticion POST
     * @return Valores por default de la estructura post
     * @throws Exception
     */
    @Override
    public FiltrosContratacionesWS obtenerValoresPorDefectoContrataciones(FiltrosContratacionesWS estructuraPost) throws Exception
    {

        if (estructuraPost.getLimite_registros() <= 0)
        {
            estructuraPost.setLimite_registros(10);
        }

        if (estructuraPost.getPagina() <= 0)
        {
            estructuraPost.setPagina(1);
        }

        return estructuraPost;

    }

    /**
     * Obtener listado de contrataciones
     *
     * @param estructuraPost Estructura para la peticion POST
     * @return Listado de contrataciones
     * @throws Exception
     */
    @Override
    public Contrataciones obtenerContratacionesPost(FiltrosContratacionesWS estructuraPost) throws Exception
    {
        return DAOWs.obtenerContratacionesPost(estructuraPost);
    }

    /**
     * Convertir a formato JSON estructura POST
     *
     * @param estructuraContrataciones Objeto que almacena la estructura del
     * sistema 6 - contrataciones
     * @return Formato JSON estructura POST
     * @throws Exception
     */
    @Override
    public String generarJsonContrataciones(Contrataciones estructuraContrataciones) throws Exception
    {

        PAGINATION atributosDePaginacion;
        ArrayList<ContratacionesContratacion> contrataciones;
        Document documentoContrataciones;
        List<Document> listaDocumentosContrataciones;

        atributosDePaginacion = estructuraContrataciones.getPagination();
        contrataciones = estructuraContrataciones.getResults();

        listaDocumentosContrataciones = new ArrayList<>();

        for (ContratacionesContratacion contratacion : contrataciones)
        {

            documentoContrataciones = new Document();

            try
            {
                Document documentoMetadata = new ContratacionesDocumentoMetadata(contratacion.getMetadata()).obtenerMetadata();
                documentoContrataciones.append("metadata", documentoMetadata);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo metadata: " + e.getMessage());
            }
            try
            {
                Document documentoPublisher = new ContratacionesDocumentoPublisher(contratacion.getPublisher()).obtenerDocumentoPublisher();
                documentoContrataciones.append("publisher", documentoPublisher);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo publisher: " + e.getMessage());
            }
            documentoContrataciones.append("cycle", contratacion.getCycle());
            documentoContrataciones.append("ocid", contratacion.getOcid());
            documentoContrataciones.append("id", contratacion.getId());
            documentoContrataciones.append("date", contratacion.getDate());
            ArrayList<String> arregloListaTags = new ContratacionesListaDocumentoTag(contratacion.getTag()).obtenerArregloDeTag();
            documentoContrataciones.append("tag", arregloListaTags);
            documentoContrataciones.append("initiationType", contratacion.getInitiationType());
            try
            {
                ArrayList<Document> arregloListaParties = new ContratacionesListaDocumentoParties(contratacion.getParties()).obtenerArregloDocumentosParties();
                documentoContrataciones.append("parties", arregloListaParties);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo parties: " + e.getMessage());
            }
            try
            {
                Document documentoBuyer = new ContratacionesDocumentoBuyer(contratacion.getBuyer()).obtenerDocumentoBuyer();
                documentoContrataciones.append("buyer", documentoBuyer);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo buyer: " + e.getMessage());
            }
            try
            {
                Document documentoPlanning = new ContratacionesDocumentoPlanning(contratacion.getPlanning()).obtenerDocumentoPlanning();
                documentoContrataciones.append("planning", documentoPlanning);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo planning: " + e.getMessage());
            }
            try
            {
                Document documentoTender = new ContratacionesDocumentoTender(contratacion.getTender()).obtenerDocumentoTender();
                documentoContrataciones.append("tender", documentoTender);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo tender: " + e.getMessage());
            }
            documentoContrataciones.append("language", contratacion.getLanguage());
            try
            {
                ArrayList<Document> arregloListaAwards = new ContratacionesListaDocumentoAwards(contratacion.getAwards()).obtenerArregloDeAwards();
                documentoContrataciones.append("awards", arregloListaAwards);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo awards: " + e.getMessage());
            }
            try
            {
                ArrayList<Document> arregloListaContracts = new ContratacionesListaDocumentoContracts(contratacion.getContracts()).obtenerArregloListaContracts();
                documentoContrataciones.append("contracts", arregloListaContracts);
            }
            catch (Exception e)
            {
                throw new Exception("Error al generar campo contracts: " + e.getMessage());
            }

            listaDocumentosContrataciones.add(documentoContrataciones);

        }

        documentoContrataciones = new Document();
        documentoContrataciones.append("pagination",
                new Document("pageSize", atributosDePaginacion.getPageSize())
                        .append("page", atributosDePaginacion.getPage())
                        .append("totalRows", atributosDePaginacion.getTotalRows())
                        .append("hasNextPage", atributosDePaginacion.getHasNextPage()));

        documentoContrataciones.append("results", listaDocumentosContrataciones);

        return documentoContrataciones.toJson();

    }

    /**
     * Convierte una cadena, en una expresion regular que contiene o no, acentos
     *
     * @param palabra
     * @return
     */
    @Override
    public String crearExpresionRegularDePalabra(String palabra)
    {

        String expReg = "";
        char[] arregloDePalabras = palabra.toLowerCase().toCharArray();

        for (char letra : arregloDePalabras)
        {
            switch (letra)
            {

                case 'a':
                    expReg += "[aáAÁ]";
                    break;

                case 'á':
                    expReg += "[aáAÁ]";
                    break;

                case 'e':
                    expReg += "[eéEÉ]";
                    break;

                case 'é':
                    expReg += "[eéEÉ]";
                    break;

                case 'i':
                    expReg += "[iíIÍ]";
                    break;

                case 'í':
                    expReg += "[iíIÍ]";
                    break;

                case 'o':
                    expReg += "[oóOÓ]";
                    break;

                case 'ó':
                    expReg += "[oóOÓ]";
                    break;

                case 'u':
                    expReg += "[uúUÚ]";
                    break;

                case 'ú':
                    expReg += "[uúUÚ]";
                    break;

                default:
                    expReg += letra;
                    break;

            }
        }

        return expReg;

    }

    /**
     * Verifica si una cadena es numerica
     *
     * @param cadena
     *
     * @return booleano
     */
    @Override
    public boolean verificarCadenaSiEsNumerica(String cadena)
    {

        if (cadena.isEmpty())
        {
            return false;
        }
        else
        {
            for (char caracter : cadena.toCharArray())
            {
                if (!Character.isDigit(caracter))
                {
                    return false;
                }
            }
        }

        return true;
    }

}
