/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariablesEjecucion;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContratacion;
import tol.sesaemm.javabeans.s6contrataciones.Contrataciones;
import tol.sesaemm.javabeans.s6contrataciones.FiltrosContratacionesWS;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOWs
{

    /**
     * Obtiene listado de Contrataciones
     *
     * @param estructuraPost Estructura para la peticion POST
     * @return Listado de contrataciones
     * @throws Exception
     */
    public static Contrataciones obtenerContratacionesPost(FiltrosContratacionesWS estructuraPost) throws Exception
    {

        MongoClient conectarBaseDatos = null;
        MongoDatabase database;
        MongoCollection<Document> coleccion;
        Collation collation;
        AggregateIterable<Document> iteradorDeResultados;
        MongoCursor<Document> cursorDeResultados = null;
        ArrayList<ContratacionesContratacion> listaContrataciones;
        Contrataciones resultadoConsulta;
        int intNumeroDocumentosEncontrados = 0;
        int intNumeroSaltos = 0;
        boolean hasNextPage = false;
        PAGINATION paginacion;
        ContratacionesContratacion contratacion;
        ObjectMapper jacksonMapper;
        ArrayList<Document> query;
        AggregateIterable<Document> iteradorDeResultadosResults;
        MongoCursor<Document> cursorDeResultadosResults = null;
        ConfVariablesEjecucion confVariablesEjecucion;

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();
            confVariablesEjecucion = new ConfVariablesEjecucion();
            listaContrataciones = new ArrayList<>();
            jacksonMapper = new ObjectMapper();
            query = new ArrayList<>();

            if (conectarBaseDatos != null)
            {

                database = conectarBaseDatos.getDatabase(confVariablesEjecucion.getBD());
                coleccion = database.getCollection("contrataciones");

                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();

                if (estructuraPost.getPagina() > 1)
                {
                    intNumeroSaltos = (int) ((estructuraPost.getPagina() - 1) * estructuraPost.getLimite_registros());
                }

                if (estructuraPost.getFechaCaptura() != null && estructuraPost.getFechaCaptura().isEmpty() == false)
                {

                    iteradorDeResultados = coleccion.aggregate(
                            Arrays.asList(
                                    new Document("$addFields", new Document()
                                            .append("fechaCapturaFiltro", new Document()
                                                    .append("$dateToString", new Document()
                                                            .append("format", "%Y-%m-%d")
                                                            .append("date", new Document()
                                                                    .append("$dateFromString", new Document("dateString", "$metadata.actualizacion")))
                                                    )
                                            )
                                    ),
                                    new Document("$match", new Document()
                                            .append("publicar",
                                                new Document("$ne", "0")
                                             )
                                            .append("fechaCapturaFiltro", new Document()
                                                    .append("$gte", estructuraPost.getFechaCaptura())
                                            )
                                    ),
                                    new Document("$count", "totalCount")
                            )
                    ).collation(collation).allowDiskUse(true);

                    query.add(
                            new Document("$addFields", new Document()
                                    .append("fechaCapturaFiltro", new Document()
                                            .append("$dateToString", new Document()
                                                    .append("format", "%Y-%m-%d")
                                                    .append("date", new Document()
                                                            .append("$dateFromString", new Document("dateString", "$metadata.actualizacion")))
                                            )
                                    )
                            )
                    );
                    query.add(
                            new Document("$match", new Document()
                                    .append("publicar",
                                        new Document("$ne", "0")
                                    )
                                    .append("fechaCapturaFiltro", new Document()
                                            .append("$gte", estructuraPost.getFechaCaptura())
                                    )
                            )
                    );

                    query.add(
                            new Document("$project", new Document()
                                    .append("_id", 0)
                                    .append("fechaCapturaFiltro", 0)
                            )
                    );
                    query.add(
                            new Document("$skip", intNumeroSaltos)
                    );
                    query.add(
                            new Document("$limit", estructuraPost.getLimite_registros())
                    );

                }
                else
                {

                    iteradorDeResultados = coleccion.aggregate(
                            Arrays.asList(
                                new Document("$match", new Document()
                                    .append("publicar",
                                        new Document("$ne", "0")
                                    )
                                ),
                                new Document("$count", "totalCount")
                            )
                    ).collation(collation).allowDiskUse(true);
                    
                    query.add(
                            new Document("$match", new Document()
                                .append("publicar",
                                        new Document("$ne", "0")
                                )
                            )
                        );
                    query.add(
                            new Document("$project", new Document()
                                    .append("_id", 0)
                            )
                    );
                    query.add(
                            new Document("$skip", intNumeroSaltos)
                    );
                    query.add(
                            new Document("$limit", estructuraPost.getLimite_registros())
                    );

                }

                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                {
                    intNumeroDocumentosEncontrados = cursorDeResultados.next().getInteger("totalCount");
                }

                iteradorDeResultadosResults = coleccion.aggregate(query);
                cursorDeResultadosResults = iteradorDeResultadosResults.iterator();

                while (cursorDeResultadosResults.hasNext())
                {
                    Document next = cursorDeResultadosResults.next();

                    contratacion = jacksonMapper.readValue(jacksonMapper.getFactory().createParser(new InputStreamReader(new ByteArrayInputStream(new Gson().toJson(next).getBytes(StandardCharsets.UTF_8)))), ContratacionesContratacion.class);

                    listaContrataciones.add(contratacion);
                }

                if ((estructuraPost.getPagina() * estructuraPost.getLimite_registros()) < intNumeroDocumentosEncontrados)
                {
                    hasNextPage = true;
                }

                paginacion = new PAGINATION();
                paginacion.setPage(estructuraPost.getPagina());
                paginacion.setPageSize(estructuraPost.getLimite_registros());
                paginacion.setTotalRows(intNumeroDocumentosEncontrados);
                paginacion.setHasNextPage(hasNextPage);

                resultadoConsulta = new Contrataciones();
                resultadoConsulta.setPagination(paginacion);
                resultadoConsulta.setResults(listaContrataciones);

            }
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener contrataciones: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);

            if (cursorDeResultadosResults != null)
            {
                cursorDeResultadosResults.close();
                cursorDeResultadosResults = null;
            }
        }

        return resultadoConsulta;

    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    {

        if (cursor != null)
        {
            cursor.close();
            cursor = null;
        }

        if (conectarBaseDatos != null)
        {
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        }

    }
}
