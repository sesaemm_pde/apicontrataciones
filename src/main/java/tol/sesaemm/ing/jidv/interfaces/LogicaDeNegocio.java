/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import tol.sesaemm.javabeans.s6contrataciones.Contrataciones;
import tol.sesaemm.javabeans.s6contrataciones.FiltrosContratacionesWS;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public interface LogicaDeNegocio
{

    public FiltrosContratacionesWS obtenerValoresPorDefectoContrataciones(FiltrosContratacionesWS estructuraPost) throws Exception;

    public Contrataciones obtenerContratacionesPost(FiltrosContratacionesWS estructuraPost) throws Exception;

    public String generarJsonContrataciones(Contrataciones estructuraContrataciones) throws Exception;

    public String crearExpresionRegularDePalabra(String palabra);

    public boolean verificarCadenaSiEsNumerica(String cadena);

}
