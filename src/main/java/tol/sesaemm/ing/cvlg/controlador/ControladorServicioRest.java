package tol.sesaemm.ing.cvlg.controlador;

import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.s6contrataciones.Contrataciones;
import tol.sesaemm.javabeans.s6contrataciones.FiltrosContratacionesWS;

@RestController
public class ControladorServicioRest
{

    /**
     * Mensaje principale del web service
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String webapp()
    {
        return "Servicio Web de conexión SESAEMM - SESNA";
    }

    /**
     * Obtiene en formato Json estructura peticion Post (atributos de paginacion
     * y resultados de consulta) de servidores publicos que intervienen en
     * contrataciones
     *
     * @param Bcuerpo Estructura Post
     * @return Cadena de estructura Post en formato Json
     * @throws java.lang.Exception
     */
    @RequestMapping(value = "/contrataciones", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String obtenerEstructuraPostFormatoJsonServidoresPublicosIntervienenContrataciones(@RequestParam MultiValueMap<String, String> Bcuerpo) throws Exception
    {

        LogicaDeNegocio logicaNegocio;
        FiltrosContratacionesWS estructuraPost;
        Contrataciones contratacion;
        Document documentoDeError;
        Document documentoFiltros = new Document();

        logicaNegocio = new LogicaDeNegocioV01();

        try
        {

            if (Bcuerpo != null)
            {
                if (Bcuerpo.getFirst("fechaCaptura") != null)
                {
                    documentoFiltros.append("fechaCaptura", Bcuerpo.getFirst("fechaCaptura"));
                }

                if (Bcuerpo.getFirst("limite_registros") != null)
                {
                    if (logicaNegocio.verificarCadenaSiEsNumerica(Bcuerpo.getFirst("limite_registros")))
                    {
                        documentoFiltros.append("limite_registros", Integer.parseInt(Bcuerpo.getFirst("limite_registros")));
                    }
                }

                if (Bcuerpo.getFirst("pagina") != null)
                {
                    if (logicaNegocio.verificarCadenaSiEsNumerica(Bcuerpo.getFirst("pagina")))
                    {
                        documentoFiltros.append("pagina", Integer.parseInt(Bcuerpo.getFirst("pagina")));
                    }
                }
            }

            estructuraPost = logicaNegocio.obtenerValoresPorDefectoContrataciones(new Gson().fromJson(new Gson().toJson(documentoFiltros), FiltrosContratacionesWS.class));
            contratacion = logicaNegocio.obtenerContratacionesPost(estructuraPost);

            return logicaNegocio.generarJsonContrataciones(contratacion);

        }
        catch (Exception ex)
        {
            documentoDeError = new Document();
            documentoDeError.append("code", "contrataciones");
            documentoDeError.append("message", "Error al obtener listado de contrataciones: " + ex.toString());

            return documentoDeError.toJson();
        }
    }

    @RequestMapping(value = "/403", produces = "application/json;charset=UTF-8")
    public String error403()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "403");
        documentoDeError.append("message", "Forbidden");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/404", produces = "application/json;charset=UTF-8")
    public String error404()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "404");
        documentoDeError.append("message", "Not Found");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/405", produces = "application/json;charset=UTF-8")
    public String error405()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "405");
        documentoDeError.append("message", "Method Not Allowed");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/500", produces = "application/json;charset=UTF-8")
    public String error500()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "500");
        documentoDeError.append("message", "Internal Server Error");

        return documentoDeError.toJson();
    }

}
