/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz
 * ismael.ortiz@sesaemm.org.mx
 */
public class OAuth2Initializer extends AbstractAnnotationConfigDispatcherServletInitializer
{

    /**
     *
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses()
    {
        return new Class[]
        {
            OAuth2Configuration.class
        };
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses()
    {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    protected String[] getServletMappings()
    {
        return new String[]
        {
            "/"
        };
    }
}
