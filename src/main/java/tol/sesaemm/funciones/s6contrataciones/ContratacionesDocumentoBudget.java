/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesBudget;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoBudget {

    private ContratacionesBudget budget;
    private Document documentoBudget;

    public ContratacionesDocumentoBudget() {
    }

    public ContratacionesDocumentoBudget(ContratacionesBudget budget) {
        this.budget = budget;
    }

    public Document obtenerDocumentoBudget() {
        documentoBudget = new Document();

        if (budget == null) {

            documentoBudget.append("id", null);

            documentoBudget.append("description", null);

            Document documentoAmount = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoBudget.append("amount", documentoAmount);

            documentoBudget.append("project", null);

            documentoBudget.append("projectID", null);

            documentoBudget.append("uri", null);

            documentoBudget.append("source", null);

        } else {

            documentoBudget.append("id", this.budget.getId());

            documentoBudget.append("description", this.budget.getDescription());

            Document documentoAmount = new ContratacionesDocumentoAmount(this.budget.getAmount())
                    .obtenerDocumentoAmount();
            documentoBudget.append("amount", documentoAmount);

            documentoBudget.append("project", this.budget.getProject());

            documentoBudget.append("projectID", this.budget.getProjectID());

            documentoBudget.append("uri", this.budget.getUri());

            documentoBudget.append("source", this.budget.getSource());

        }

        return documentoBudget;

    }

}
