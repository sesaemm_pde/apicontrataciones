/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.METADATA;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoMetadata {
    private METADATA metadata;

    public ContratacionesDocumentoMetadata() {
    }

    public ContratacionesDocumentoMetadata(METADATA metadata) {
        this.metadata = metadata;
    }

    public Document obtenerMetadata() {

        Document documentoMetadata = new Document();

        documentoMetadata.append("actualizacion", this.metadata.getActualizacion());
        documentoMetadata.append("institucion", this.metadata.getInstitucion());
        documentoMetadata.append("contacto", this.metadata.getContacto());
        documentoMetadata.append("personaContacto", this.metadata.getPersonaContacto());

        return documentoMetadata;
    }
}
