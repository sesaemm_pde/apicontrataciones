/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesParties;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoParties {

    private ArrayList<ContratacionesParties> parties;
    private ArrayList<Document> arregloListaParties;

    public ContratacionesListaDocumentoParties() {
    }

    public ContratacionesListaDocumentoParties(ArrayList<ContratacionesParties> parties) {
        this.parties = parties;
    }

    public ArrayList<Document> obtenerArregloDocumentosParties() {
        arregloListaParties = new ArrayList<>();

        if (parties == null || parties.isEmpty() == true) {
            Document documentoParties = new Document();

            documentoParties.append("name", null);

            documentoParties.append("id", null);

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(null).obtenerDocumentoIdentifier();
            documentoParties.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    null).obtenerArregloDocumnetosIdentifiers();
            documentoParties.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoAddress = new ContratacionesDocumentoAddress(null).obtenerDocumentoAddress();
            documentoParties.append("address", documentoAddress);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(null)
                    .obtenerDocumentoContactPoint();
            documentoParties.append("contactPoint", documentoContactPoint);

            ArrayList<String> arregloListaRoles = new ContratacionesListaDocumentoRoles(null).obtenerArregloDeRoles();
            documentoParties.append("roles", arregloListaRoles);

            Document documentoDetails = new Document();

            documentoDetails.append("scale", null);

            documentoParties.append("details", documentoDetails);

            arregloListaParties.add(documentoParties);
        } else {

            for (int i = 0; i < this.parties.size(); i++) {

                Document documentoParties = new Document();

                documentoParties.append("name", this.parties.get(i).getName());

                documentoParties.append("id", this.parties.get(i).getId());

                Document documentoIdentifier = new ContratacionesDocumentoIdentifier(
                        this.parties.get(i).getIdentifier()).obtenerDocumentoIdentifier();
                documentoParties.append("identifier", documentoIdentifier);

                ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                        this.parties.get(i).getAdditionalIdentifiers()).obtenerArregloDocumnetosIdentifiers();
                documentoParties.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

                Document documentoAddress = new ContratacionesDocumentoAddress(this.parties.get(i).getAddress())
                        .obtenerDocumentoAddress();
                documentoParties.append("address", documentoAddress);

                Document documentoContactPoint = new ContratacionesDocumentoContactPoint(
                        this.parties.get(i).getContactPoint()).obtenerDocumentoContactPoint();
                documentoParties.append("contactPoint", documentoContactPoint);

                ArrayList<String> arregloListaRoles = new ContratacionesListaDocumentoRoles(
                        this.parties.get(i).getRoles()).obtenerArregloDeRoles();
                documentoParties.append("roles", arregloListaRoles);

                if (parties.get(i).getDetails() == null) {
                    Document documentoDetails = new Document();

                    documentoDetails.append("scale", null);

                    documentoParties.append("details", documentoDetails);
                } else {
                    Document documentoDetails = new Document();

                    documentoDetails.append("scale", this.parties.get(i).getDetails().getScale());

                    documentoParties.append("details", documentoDetails);
                }

                arregloListaParties.add(documentoParties);

            }

        }

        return arregloListaParties;
    }

}
