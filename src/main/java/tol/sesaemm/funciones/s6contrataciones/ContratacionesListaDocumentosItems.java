/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesItems;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentosItems {

    private ArrayList<ContratacionesItems> items;
    private ArrayList<Document> arregloListaItems;

    public ContratacionesListaDocumentosItems() {
    }

    public ContratacionesListaDocumentosItems(ArrayList<ContratacionesItems> items) {
        this.items = items;
    }

    public ArrayList<Document> obtenerArregloDeItems() {
        arregloListaItems = new ArrayList<>();

        if (this.items == null || this.items.isEmpty() == true) {
            Document documentoItem = new Document();

            documentoItem.append("id", null);

            documentoItem.append("description", null);

            Document documentoClassification = new Document();

            documentoClassification.append("scheme", null);

            documentoClassification.append("id", null);

            documentoClassification.append("description", null);

            documentoClassification.append("uri", null);

            documentoItem.append("classification", documentoClassification);

            ArrayList<Document> arregloDocumentosAdditionalClassifications = new ContratacionesListaDocumentoAdditionalClassifications(
                    null).obtenerArregloDocumentosClassification();
            documentoItem.append("additionalClassifications", arregloDocumentosAdditionalClassifications);

            documentoItem.append("quantity", null);

            Document documentoUnit = new Document();

            documentoUnit.append("scheme", null);

            documentoUnit.append("id", null);

            documentoUnit.append("name", null);

            Document documentoValue = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoUnit.append("value", documentoValue);

            documentoUnit.append("uri", null);

            documentoItem.append("unit", documentoUnit);

            arregloListaItems.add(documentoItem);
        } else {

            for (int i = 0; i < this.items.size(); i++) {
                Document documentoItem = new Document();

                documentoItem.append("id", this.items.get(i).getId());

                documentoItem.append("description", this.items.get(i).getDescription());

                if (this.items.get(i).getClassification() == null) {
                    Document documentoClassification = new Document();

                    documentoClassification.append("scheme", null);

                    documentoClassification.append("id", null);

                    documentoClassification.append("description", null);

                    documentoClassification.append("uri", null);

                    documentoItem.append("classification", documentoClassification);
                } else {
                    Document documentoClassification = new Document();

                    documentoClassification.append("scheme", this.items.get(i).getClassification().getScheme());

                    documentoClassification.append("id", this.items.get(i).getClassification().getId());

                    documentoClassification.append("description",
                            this.items.get(i).getClassification().getDescription());

                    documentoClassification.append("uri", this.items.get(i).getClassification().getUri());

                    documentoItem.append("classification", documentoClassification);
                }

                ArrayList<Document> arregloDocumentosAdditionalClassifications = new ContratacionesListaDocumentoAdditionalClassifications(
                        this.items.get(i).getAdditionalClassifications()).obtenerArregloDocumentosClassification();
                documentoItem.append("additionalClassifications", arregloDocumentosAdditionalClassifications);

                documentoItem.append("quantity", this.items.get(i).getQuantity());

                if (this.items.get(i).getUnit() == null) {
                    Document documentoUnit = new Document();

                    documentoUnit.append("scheme", null);

                    documentoUnit.append("id", null);

                    documentoUnit.append("name", null);

                    Document documentoValue = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
                    documentoUnit.append("value", documentoValue);

                    documentoUnit.append("uri", null);

                    documentoItem.append("unit", documentoUnit);
                } else {
                    Document documentoUnit = new Document();

                    documentoUnit.append("scheme", this.items.get(i).getUnit().getScheme());

                    documentoUnit.append("id", this.items.get(i).getUnit().getId());

                    documentoUnit.append("name", this.items.get(i).getUnit().getName());

                    Document documentoValue = new ContratacionesDocumentoAmount(this.items.get(i).getUnit().getValue())
                            .obtenerDocumentoAmount();
                    documentoUnit.append("value", documentoValue);

                    documentoUnit.append("uri", this.items.get(i).getUnit().getUri());

                    documentoItem.append("unit", documentoUnit);
                }

                arregloListaItems.add(documentoItem);

            }

        }

        return arregloListaItems;

    }

}
