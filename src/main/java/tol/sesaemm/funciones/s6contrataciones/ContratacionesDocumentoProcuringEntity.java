/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesProcuringEntity;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoProcuringEntity {

    private ContratacionesProcuringEntity procuringEntity;
    private Document documentoProcuringEntity;

    public ContratacionesDocumentoProcuringEntity() {
    }

    public ContratacionesDocumentoProcuringEntity(ContratacionesProcuringEntity procuringEntity) {
        this.procuringEntity = procuringEntity;

    }

    public Document obtenerDocumentoProcuringEntity() {
        documentoProcuringEntity = new Document();

        if (this.procuringEntity == null) {

            documentoProcuringEntity.append("name", null);

            documentoProcuringEntity.append("id", null);

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(null).obtenerDocumentoIdentifier();
            documentoProcuringEntity.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    null).obtenerArregloDocumnetosIdentifiers();
            documentoProcuringEntity.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(null)
                    .obtenerDocumentoContactPoint();
            documentoProcuringEntity.append("contactPoint", documentoContactPoint);

            Document documentoAddress = new ContratacionesDocumentoAddress(null).obtenerDocumentoAddress();
            documentoProcuringEntity.append("address", documentoAddress);

        } else {

            documentoProcuringEntity.append("name", this.procuringEntity.getName());

            documentoProcuringEntity.append("id", this.procuringEntity.getId());

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(this.procuringEntity.getIdentifier())
                    .obtenerDocumentoIdentifier();
            documentoProcuringEntity.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    this.procuringEntity.getAdditionalIdentifiers()).obtenerArregloDocumnetosIdentifiers();
            documentoProcuringEntity.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(
                    this.procuringEntity.getContactPoint()).obtenerDocumentoContactPoint();
            documentoProcuringEntity.append("contactPoint", documentoContactPoint);

            Document documentoAddress = new ContratacionesDocumentoAddress(this.procuringEntity.getAddress())
                    .obtenerDocumentoAddress();
            documentoProcuringEntity.append("address", documentoAddress);

        }

        return documentoProcuringEntity;

    }

}
