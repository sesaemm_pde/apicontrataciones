/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTenderers;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoTenderers {

    private ArrayList<ContratacionesTenderers> tenderers;
    private ArrayList<Document> arregloListaTenderers;

    public ContratacionesListaDocumentoTenderers() {
    }

    public ContratacionesListaDocumentoTenderers(ArrayList<ContratacionesTenderers> tenderers) {
        this.tenderers = tenderers;
    }

    public ArrayList<Document> obtenerArregloDeTenderers() {
        arregloListaTenderers = new ArrayList<>();

        if (this.tenderers == null || this.tenderers.isEmpty() == true) {
            Document documentoTenderer = new Document();

            documentoTenderer.append("name", null);

            documentoTenderer.append("id", null);

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(null).obtenerDocumentoIdentifier();
            documentoTenderer.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    null).obtenerArregloDocumnetosIdentifiers();
            documentoTenderer.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(null)
                    .obtenerDocumentoContactPoint();
            documentoTenderer.append("contactPoint", documentoContactPoint);

            Document documentoAddress = new ContratacionesDocumentoAddress(null).obtenerDocumentoAddress();
            documentoTenderer.append("address", documentoAddress);

            arregloListaTenderers.add(documentoTenderer);
        } else {

            for (int i = 0; i < this.tenderers.size(); i++) {
                Document documentoTenderer = new Document();

                documentoTenderer.append("name", this.tenderers.get(i).getName());

                documentoTenderer.append("id", this.tenderers.get(i).getId());

                Document documentoIdentifier = new ContratacionesDocumentoIdentifier(
                        this.tenderers.get(i).getIdentifier()).obtenerDocumentoIdentifier();
                documentoTenderer.append("identifier", documentoIdentifier);

                ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                        this.tenderers.get(i).getAdditionalIdentifiers()).obtenerArregloDocumnetosIdentifiers();
                documentoTenderer.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

                Document documentoContactPoint = new ContratacionesDocumentoContactPoint(
                        this.tenderers.get(i).getContactPoint()).obtenerDocumentoContactPoint();
                documentoTenderer.append("contactPoint", documentoContactPoint);

                Document documentoAddress = new ContratacionesDocumentoAddress(this.tenderers.get(i).getAddress())
                        .obtenerDocumentoAddress();
                documentoTenderer.append("address", documentoAddress);

                arregloListaTenderers.add(documentoTenderer);

            }

        }

        return arregloListaTenderers;

    }

}
