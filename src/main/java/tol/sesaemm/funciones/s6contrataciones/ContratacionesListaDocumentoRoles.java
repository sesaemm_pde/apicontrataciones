/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoRoles {

    private ArrayList<String> roles;
    private ArrayList<String> arregloListaRoles;

    public ContratacionesListaDocumentoRoles() {
    }

    public ContratacionesListaDocumentoRoles(ArrayList<String> roles) {
        this.roles = roles;
    }

    public ArrayList<String> obtenerArregloDeRoles() {

        arregloListaRoles = new ArrayList<>();

        if (this.roles == null || this.roles.isEmpty() == true) {
            arregloListaRoles.add(null);
        } else {

            for (int i = 0; i < roles.size(); i++) {
                arregloListaRoles.add(roles.get(i));
            }

        }

        return arregloListaRoles;

    }

}
