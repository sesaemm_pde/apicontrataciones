/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAmendments;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoAmendments {

    private ArrayList<ContratacionesAmendments> amendments;
    private ArrayList<Document> arregloListaAmendments;

    public ContratacionesListaDocumentoAmendments() {
    }

    public ContratacionesListaDocumentoAmendments(ArrayList<ContratacionesAmendments> amendments) {
        this.amendments = amendments;
    }

    public ArrayList<Document> obtenerArregloDeAmendments() {
        arregloListaAmendments = new ArrayList<>();

        if (amendments == null || amendments.isEmpty() == true) {
            Document documentoAmendments = new Document();

            documentoAmendments.append("date", null);

            documentoAmendments.append("rationale", null);

            documentoAmendments.append("id", null);

            documentoAmendments.append("description", null);

            documentoAmendments.append("amendsReleaseID", null);

            documentoAmendments.append("releaseID", null);

            ArrayList<Document> arregloListaChanges = new ContratacionesListaDocumentoChanges(null)
                    .obtenerArregloDeChanges();
            documentoAmendments.append("changes", arregloListaChanges);

            arregloListaAmendments.add(documentoAmendments);
        } else {

            for (int i = 0; i < amendments.size(); i++) {
                Document documentoAmendments = new Document();

                documentoAmendments.append("date", amendments.get(i).getDate());

                documentoAmendments.append("rationale", amendments.get(i).getRationale());

                documentoAmendments.append("id", amendments.get(i).getId());

                documentoAmendments.append("description", amendments.get(i).getDescription());

                documentoAmendments.append("amendsReleaseID", amendments.get(i).getAmendsReleaseID());

                documentoAmendments.append("releaseID", amendments.get(i).getReleaseID());

                ArrayList<Document> arregloListaChanges = new ContratacionesListaDocumentoChanges(
                        this.amendments.get(i).getChanges()).obtenerArregloDeChanges();
                documentoAmendments.append("changes", arregloListaChanges);

                arregloListaAmendments.add(documentoAmendments);

            }

        }

        return arregloListaAmendments;

    }

}
