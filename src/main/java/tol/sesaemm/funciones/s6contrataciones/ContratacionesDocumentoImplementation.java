/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesImplementation;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoImplementation {

    private ContratacionesImplementation implementation;
    private Document documentoImplementation;

    public ContratacionesDocumentoImplementation() {
    }

    public ContratacionesDocumentoImplementation(ContratacionesImplementation implementation) {
        this.implementation = implementation;
    }

    public Document obtenerDocumentoImplementation() {
        documentoImplementation = new Document();

        if (implementation == null) {

            ArrayList<Document> arregloListaTransactions = new ContratacionesListaDocumentoTransactions(null)
                    .obtenerArregloDeTransactions();
            documentoImplementation.append("transactions", arregloListaTransactions);

            ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(null)
                    .obtenerArregloDocumnetoMilestones();
            documentoImplementation.append("milestones", arregloListaMilestones);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(null)
                    .obtenerArregloDocumnetoDocuments();
            documentoImplementation.append("documents", arregloListaDocuments);

        } else {

            ArrayList<Document> arregloListaTransactions = new ContratacionesListaDocumentoTransactions(
                    this.implementation.getTransactions()).obtenerArregloDeTransactions();
            documentoImplementation.append("transactions", arregloListaTransactions);

            ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(
                    this.implementation.getMilestones()).obtenerArregloDocumnetoMilestones();
            documentoImplementation.append("milestones", arregloListaMilestones);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(
                    this.implementation.getDocuments()).obtenerArregloDocumnetoDocuments();
            documentoImplementation.append("documents", arregloListaDocuments);

        }

        return documentoImplementation;

    }

}
