/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoSubmissionMethod {

    private ArrayList<String> submissionMethod;
    private ArrayList<String> arregloListaSubmissionMethod;

    public ContratacionesListaDocumentoSubmissionMethod() {
    }

    public ContratacionesListaDocumentoSubmissionMethod(ArrayList<String> submissionMethod) {
        this.submissionMethod = submissionMethod;
    }

    public ArrayList<String> obtenerArregloDeSubmissionMethod() {

        arregloListaSubmissionMethod = new ArrayList<>();

        if (this.submissionMethod == null || this.submissionMethod.isEmpty() == true) {
            arregloListaSubmissionMethod.add(null);
        } else {

            for (int i = 0; i < this.submissionMethod.size(); i++) {
                arregloListaSubmissionMethod.add(this.submissionMethod.get(i));
            }

        }

        return arregloListaSubmissionMethod;

    }

}
