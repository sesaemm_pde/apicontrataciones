/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoRelationship {

    private ArrayList<String> relationship;
    private ArrayList<String> arregloListaRelationship;

    public ContratacionesListaDocumentoRelationship() {
    }

    public ContratacionesListaDocumentoRelationship(ArrayList<String> relationship) {
        this.relationship = relationship;
    }

    public ArrayList<String> obtenerArregloDeRelationship() {

        arregloListaRelationship = new ArrayList<>();

        if (this.relationship == null || this.relationship.isEmpty() == true) {
            arregloListaRelationship.add(null);
        } else {

            for (int i = 0; i < this.relationship.size(); i++) {
                arregloListaRelationship.add(this.relationship.get(i));
            }

        }

        return arregloListaRelationship;

    }

}
