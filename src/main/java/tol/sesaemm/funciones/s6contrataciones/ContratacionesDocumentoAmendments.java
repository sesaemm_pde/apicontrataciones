/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAmendments;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoAmendments {

    private ContratacionesAmendments amendments;
    private Document documentoAmendments;

    public ContratacionesDocumentoAmendments() {
    }

    public ContratacionesDocumentoAmendments(ContratacionesAmendments amendments) {
        this.amendments = amendments;
    }

    public Document obtenerDocumentoAmendments() {
        documentoAmendments = new Document();

        if (this.amendments == null) {
            documentoAmendments.append("date", null);
            documentoAmendments.append("rationale", null);
            documentoAmendments.append("id", null);
            documentoAmendments.append("description", null);
            documentoAmendments.append("amendsReleaseID", null);
            documentoAmendments.append("releaseID", null);
            ArrayList<Document> arregloListaChanges = new ContratacionesListaDocumentoChanges(null)
                    .obtenerArregloDeChanges();
            documentoAmendments.append("changes", arregloListaChanges);
        } else {
            documentoAmendments.append("date", this.amendments.getDate());
            documentoAmendments.append("rationale", this.amendments.getRationale());
            documentoAmendments.append("id", this.amendments.getId());
            documentoAmendments.append("description", this.amendments.getDescription());
            documentoAmendments.append("amendsReleaseID", this.amendments.getAmendsReleaseID());
            documentoAmendments.append("releaseID", this.amendments.getReleaseID());
            ArrayList<Document> arregloListaChanges = new ContratacionesListaDocumentoChanges(
                    this.amendments.getChanges()).obtenerArregloDeChanges();
            documentoAmendments.append("changes", arregloListaChanges);
        }

        return documentoAmendments;

    }

}