/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesDocuments;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoDocuments {

    private ArrayList<ContratacionesDocuments> documents;
    private ArrayList<Document> arregloListaDocuments;

    public ContratacionesListaDocumentoDocuments() {
    }

    public ContratacionesListaDocumentoDocuments(ArrayList<ContratacionesDocuments> documents) {
        this.documents = documents;
    }

    public ArrayList<Document> obtenerArregloDocumnetoDocuments() {
        arregloListaDocuments = new ArrayList<>();

        if (this.documents == null || this.documents.isEmpty() == true) {
            Document documentoDocument = new Document();

            documentoDocument.append("id", null);

            documentoDocument.append("documentType", null);

            documentoDocument.append("title", null);

            documentoDocument.append("description", null);

            documentoDocument.append("url", null);

            documentoDocument.append("datePublished", null);

            documentoDocument.append("dateModified", null);

            documentoDocument.append("format", null);

            documentoDocument.append("language", null);

            arregloListaDocuments.add(documentoDocument);
        } else {

            for (int i = 0; i < this.documents.size(); i++) {
                Document documentoDocument = new Document();

                documentoDocument.append("id", this.documents.get(i).getId());

                documentoDocument.append("documentType", this.documents.get(i).getDocumentType());

                documentoDocument.append("title", this.documents.get(i).getTitle());

                documentoDocument.append("description", this.documents.get(i).getDescription());

                documentoDocument.append("url", this.documents.get(i).getUrl());

                documentoDocument.append("datePublished", this.documents.get(i).getDatePublished());

                documentoDocument.append("dateModified", this.documents.get(i).getDateModified());

                documentoDocument.append("format", this.documents.get(i).getFormat());

                documentoDocument.append("language", this.documents.get(i).getLanguage());

                arregloListaDocuments.add(documentoDocument);

            }

        }

        return arregloListaDocuments;
    }

}
