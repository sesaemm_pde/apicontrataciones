/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoAdditionalProcurementCategories {

    private ArrayList<String> additionalProcurementCategories;
    private ArrayList<String> arregloListaAdditionalProcurementCategories;

    public ContratacionesListaDocumentoAdditionalProcurementCategories() {
    }

    public ContratacionesListaDocumentoAdditionalProcurementCategories(
            ArrayList<String> additionalProcurementCategories) {
        this.additionalProcurementCategories = additionalProcurementCategories;
    }

    public ArrayList<String> obtenerArregloDeAdditionalProcurementCategories() {

        arregloListaAdditionalProcurementCategories = new ArrayList<>();

        if (additionalProcurementCategories == null || additionalProcurementCategories.isEmpty() == true) {
            arregloListaAdditionalProcurementCategories.add(null);
        } else {
            for (int i = 0; i < additionalProcurementCategories.size(); i++) {
                arregloListaAdditionalProcurementCategories.add(additionalProcurementCategories.get(i));
            }
        }

        return arregloListaAdditionalProcurementCategories;

    }

}
