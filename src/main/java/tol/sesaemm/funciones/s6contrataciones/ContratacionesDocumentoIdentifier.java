/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesIdentifier;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoIdentifier {

    private ContratacionesIdentifier identifier;
    private Document documentoIdentifier;

    public ContratacionesDocumentoIdentifier() {
    }

    public ContratacionesDocumentoIdentifier(ContratacionesIdentifier identifier) {
        this.identifier = identifier;
    }

    public Document obtenerDocumentoIdentifier() {
        documentoIdentifier = new Document();

        if (this.identifier == null) {

            documentoIdentifier.append("scheme", null);

            documentoIdentifier.append("id", null);

            documentoIdentifier.append("legalName", null);

            documentoIdentifier.append("uri", null);

        } else {

            documentoIdentifier.append("scheme", this.identifier.getScheme());

            documentoIdentifier.append("id", this.identifier.getId());

            documentoIdentifier.append("legalName", this.identifier.getLegalName());

            documentoIdentifier.append("uri", this.identifier.getUri());

        }

        return documentoIdentifier;

    }

}
