/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesClassification;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoAdditionalClassifications {

    private ArrayList<ContratacionesClassification> classification;
    private ArrayList<Document> arregloListaClassification;

    public ContratacionesListaDocumentoAdditionalClassifications() {
    }

    public ContratacionesListaDocumentoAdditionalClassifications(
            ArrayList<ContratacionesClassification> classification) {
        this.classification = classification;
    }

    public ArrayList<Document> obtenerArregloDocumentosClassification() {
        arregloListaClassification = new ArrayList<>();

        if (this.classification == null || this.classification.isEmpty() == true) {
            Document documentoClassification = new Document();

            documentoClassification.append("scheme", null);

            documentoClassification.append("id", null);

            documentoClassification.append("description", null);

            documentoClassification.append("uri", null);

            arregloListaClassification.add(documentoClassification);
        } else {

            for (int i = 0; i < this.classification.size(); i++) {
                Document documentoClassification = new Document();

                documentoClassification.append("scheme", this.classification.get(i).getScheme());

                documentoClassification.append("id", this.classification.get(i).getId());

                documentoClassification.append("description", this.classification.get(i).getDescription());

                documentoClassification.append("uri", this.classification.get(i).getUri());

                arregloListaClassification.add(documentoClassification);

            }

        }

        return arregloListaClassification;

    }

}
