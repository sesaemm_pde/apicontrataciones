/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesChanges;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoChanges
{
    private ArrayList<ContratacionesChanges> changes;
    private ArrayList<Document> arregloListaChanges;
    
    public ContratacionesListaDocumentoChanges()    
    {
    }
    
    public ContratacionesListaDocumentoChanges(ArrayList<ContratacionesChanges> changes) {
        this.changes = changes;
    }
    
    public ArrayList<Document> obtenerArregloDeChanges()
    {
        arregloListaChanges = new ArrayList<>();

        if (changes == null || changes.isEmpty() == true)
        {
            Document documentoChanges = new Document();

            documentoChanges.append("property", null);
            documentoChanges.append("former_value", null);

            arregloListaChanges.add(documentoChanges);
        }
        else
        {

            for (int i = 0; i < changes.size(); i++)
            {
                Document documentoChanges = new Document();

                documentoChanges.append("property", changes.get(i).getProperty());
                documentoChanges.append("former_value", changes.get(i).getFormer_value());
                
                arregloListaChanges.add(documentoChanges);
                
            }

        }
        
        return arregloListaChanges;

    }
}