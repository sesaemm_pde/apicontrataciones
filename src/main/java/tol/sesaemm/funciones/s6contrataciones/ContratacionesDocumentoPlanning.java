/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesPlanning;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoPlanning {

    private ContratacionesPlanning planning;
    private Document documentoPlanning;

    public ContratacionesDocumentoPlanning() {
    }

    public ContratacionesDocumentoPlanning(ContratacionesPlanning planning) {
        this.planning = planning;
    }

    public Document obtenerDocumentoPlanning() {
        documentoPlanning = new Document();

        if (planning == null) {

            documentoPlanning.append("rationale", null);

            Document documentoBudget = new ContratacionesDocumentoBudget(null).obtenerDocumentoBudget();
            documentoPlanning.append("budget", documentoBudget);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(null)
                    .obtenerArregloDocumnetoDocuments();
            documentoPlanning.append("documents", arregloListaDocuments);

            ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(null)
                    .obtenerArregloDocumnetoMilestones();
            documentoPlanning.append("milestones", arregloListaMilestones);

        } else {

            documentoPlanning.append("rationale", this.planning.getRationale());

            Document documentoBudget = new ContratacionesDocumentoBudget(this.planning.getBudget())
                    .obtenerDocumentoBudget();
            documentoPlanning.append("budget", documentoBudget);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(
                    this.planning.getDocuments()).obtenerArregloDocumnetoDocuments();
            documentoPlanning.append("documents", arregloListaDocuments);

            ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(
                    this.planning.getMilestones()).obtenerArregloDocumnetoMilestones();
            documentoPlanning.append("milestones", arregloListaMilestones);

        }

        return documentoPlanning;

    }

}
