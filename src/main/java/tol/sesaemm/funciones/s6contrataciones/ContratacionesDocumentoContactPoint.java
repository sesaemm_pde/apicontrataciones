/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContactPoint;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoContactPoint {

    private ContratacionesContactPoint contactPoint;
    private Document documentoContactPoint;

    public ContratacionesDocumentoContactPoint() {
    }

    public ContratacionesDocumentoContactPoint(ContratacionesContactPoint contactPoint) {
        this.contactPoint = contactPoint;
    }

    public Document obtenerDocumentoContactPoint() {
        documentoContactPoint = new Document();

        if (this.contactPoint == null) {

            documentoContactPoint.append("name", null);

            documentoContactPoint.append("email", null);

            documentoContactPoint.append("telephone", null);

            documentoContactPoint.append("faxNumber", null);

            documentoContactPoint.append("url", null);

        } else {

            documentoContactPoint.append("name", this.contactPoint.getName());

            documentoContactPoint.append("email", this.contactPoint.getEmail());

            documentoContactPoint.append("telephone", this.contactPoint.getTelephone());

            documentoContactPoint.append("faxNumber", this.contactPoint.getFaxNumber());

            documentoContactPoint.append("url", this.contactPoint.getUrl());

        }

        return documentoContactPoint;

    }

}
