/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAmount;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoAmount {

    private ContratacionesAmount amount;
    private Document documentoAmount;

    public ContratacionesDocumentoAmount() {
    }

    public ContratacionesDocumentoAmount(ContratacionesAmount amount) {
        this.amount = amount;
    }

    public Document obtenerDocumentoAmount() {
        documentoAmount = new Document();

        if (this.amount == null) {

            documentoAmount.append("amount", null);

            documentoAmount.append("currency", null);

        } else {

            documentoAmount.append("amount", this.amount.getAmount());

            documentoAmount.append("currency", this.amount.getCurrency());

        }

        return documentoAmount;

    }

}
