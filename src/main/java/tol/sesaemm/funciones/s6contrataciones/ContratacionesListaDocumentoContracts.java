/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContracts;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoContracts {

    private ArrayList<ContratacionesContracts> contracts;
    private ArrayList<Document> arregloListaContracts;

    public ContratacionesListaDocumentoContracts() {
    }

    public ContratacionesListaDocumentoContracts(ArrayList<ContratacionesContracts> contracts) {
        this.contracts = contracts;
    }

    public ArrayList<Document> obtenerArregloListaContracts() {
        arregloListaContracts = new ArrayList<>();

        if (this.contracts == null || this.contracts.isEmpty() == true) {
            Document documentoContracts = new Document();

            documentoContracts.append("id", null);

            documentoContracts.append("awardID", null);

            documentoContracts.append("title", null);

            documentoContracts.append("description", null);

            documentoContracts.append("status", null);

            Document documentoPeriod = new ContratacionesDocumentoTenderPeriod(null).obtenerDocumentoTenderPeriod();
            documentoContracts.append("period", documentoPeriod);

            Document documentoValue = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoContracts.append("value", documentoValue);

            ArrayList<Document> arregloListaItems = new ContratacionesListaDocumentosItems(null)
                    .obtenerArregloDeItems();
            documentoContracts.append("items", arregloListaItems);

            documentoContracts.append("dateSigned", null);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(null)
                    .obtenerArregloDocumnetoDocuments();
            documentoContracts.append("documents", arregloListaDocuments);

            Document documentoImplementation = new ContratacionesDocumentoImplementation(null)
                    .obtenerDocumentoImplementation();
            documentoContracts.append("implementation", documentoImplementation);

            ArrayList<Document> arregloListaRelatedProcesses = new ContratacionesListaDocumentoRelatedProcesses(null)
                    .obtenerArregloDeRelatedProcesses();
            documentoContracts.append("relatedProcesses", arregloListaRelatedProcesses);

            ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(null)
                    .obtenerArregloDocumnetoMilestones();
            documentoContracts.append("milestones", arregloListaMilestones);

            ArrayList<Document> arregloListaAmendments = new ContratacionesListaDocumentoAmendments(null)
                    .obtenerArregloDeAmendments();
            documentoContracts.append("amendments", arregloListaAmendments);

            Document documentoAmendment = new ContratacionesDocumentoAmendments(null).obtenerDocumentoAmendments();
            documentoContracts.append("amendment", documentoAmendment);

            arregloListaContracts.add(documentoContracts);
        } else {

            for (int i = 0; i < this.contracts.size(); i++) {
                Document documentoContracts = new Document();

                documentoContracts.append("id", this.contracts.get(i).getId());

                documentoContracts.append("awardID", this.contracts.get(i).getAwardID());

                documentoContracts.append("title", this.contracts.get(i).getTitle());

                documentoContracts.append("description", this.contracts.get(i).getDescription());

                documentoContracts.append("status", this.contracts.get(i).getStatus());

                Document documentoPeriod = new ContratacionesDocumentoTenderPeriod(this.contracts.get(i).getPeriod())
                        .obtenerDocumentoTenderPeriod();
                documentoContracts.append("period", documentoPeriod);

                Document documentoValue = new ContratacionesDocumentoAmount(this.contracts.get(i).getValue())
                        .obtenerDocumentoAmount();
                documentoContracts.append("value", documentoValue);

                ArrayList<Document> arregloListaItems = new ContratacionesListaDocumentosItems(
                        this.contracts.get(i).getItems()).obtenerArregloDeItems();
                documentoContracts.append("items", arregloListaItems);

                documentoContracts.append("dateSigned", this.contracts.get(i).getDateSigned());

                ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(
                        this.contracts.get(i).getDocuments()).obtenerArregloDocumnetoDocuments();
                documentoContracts.append("documents", arregloListaDocuments);

                Document documentoImplementation = new ContratacionesDocumentoImplementation(
                        this.contracts.get(i).getImplementation()).obtenerDocumentoImplementation();
                documentoContracts.append("implementation", documentoImplementation);

                ArrayList<Document> arregloListaRelatedProcesses = new ContratacionesListaDocumentoRelatedProcesses(
                        this.contracts.get(i).getRelatedProcesses()).obtenerArregloDeRelatedProcesses();
                documentoContracts.append("relatedProcesses", arregloListaRelatedProcesses);

                ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(
                        this.contracts.get(i).getMilestones()).obtenerArregloDocumnetoMilestones();
                documentoContracts.append("milestones", arregloListaMilestones);

                ArrayList<Document> arregloListaAmendments = new ContratacionesListaDocumentoAmendments(
                        this.contracts.get(i).getAmendments()).obtenerArregloDeAmendments();
                documentoContracts.append("amendments", arregloListaAmendments);

                Document documentoAmendment = new ContratacionesDocumentoAmendments(
                        this.contracts.get(i).getAmendment()).obtenerDocumentoAmendments();
                documentoContracts.append("amendment", documentoAmendment);

                arregloListaContracts.add(documentoContracts);

            }

        }

        return arregloListaContracts;

    }

}
