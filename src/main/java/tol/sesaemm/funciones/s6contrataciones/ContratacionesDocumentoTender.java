/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTender;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoTender {

    private ContratacionesTender tender;
    private Document documentoTender;

    public ContratacionesDocumentoTender() {
    }

    public ContratacionesDocumentoTender(ContratacionesTender tender) {
        this.tender = tender;
    }

    public Document obtenerDocumentoTender() {
        documentoTender = new Document();

        if (tender == null) {

            documentoTender.append("id", null);

            documentoTender.append("title", null);

            documentoTender.append("description", null);

            documentoTender.append("status", null);

            Document documentoProcuringEntity = new ContratacionesDocumentoProcuringEntity(null)
                    .obtenerDocumentoProcuringEntity();
            documentoTender.append("procuringEntity", documentoProcuringEntity);

            ArrayList<Document> arregloListaItems = new ContratacionesListaDocumentosItems(null)
                    .obtenerArregloDeItems();
            documentoTender.append("items", arregloListaItems);

            Document documentoValue = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoTender.append("value", documentoValue);

            Document documentoMinValue = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoTender.append("minValue", documentoMinValue);

            documentoTender.append("procurementMethod", null);

            documentoTender.append("procurementMethodDetails", null);

            documentoTender.append("procurementMethodRationale", null);

            documentoTender.append("mainProcurementCategory", null);

            ArrayList<String> arregloListAadditionalProcurementCategories = new ContratacionesListaDocumentoAdditionalProcurementCategories(
                    null).obtenerArregloDeAdditionalProcurementCategories();
            documentoTender.append("additionalProcurementCategories", arregloListAadditionalProcurementCategories);

            documentoTender.append("awardCriteria", null);

            documentoTender.append("awardCriteriaDetails", null);

            ArrayList<String> arregloListAdditionalProcurementCategories = new ContratacionesListaDocumentoSubmissionMethod(
                    null).obtenerArregloDeSubmissionMethod();
            documentoTender.append("submissionMethod", arregloListAdditionalProcurementCategories);

            documentoTender.append("submissionMethodDetails", null);

            Document documentoTenderPeriod = new ContratacionesDocumentoTenderPeriod(null)
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("tenderPeriod", documentoTenderPeriod);

            Document documentoEnquiryPeriod = new ContratacionesDocumentoTenderPeriod(null)
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("enquiryPeriod", documentoEnquiryPeriod);

            documentoTender.append("hasEnquiries", null);

            documentoTender.append("eligibilityCriteria", null);

            Document documentoAwardPeriod = new ContratacionesDocumentoTenderPeriod(null)
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("awardPeriod", documentoAwardPeriod);

            Document documentoContractPeriod = new ContratacionesDocumentoTenderPeriod(null)
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("contractPeriod", documentoContractPeriod);

            documentoTender.append("numberOfTenderers", null);

            ArrayList<Document> arregloListaTenderers = new ContratacionesListaDocumentoTenderers(null)
                    .obtenerArregloDeTenderers();
            documentoTender.append("tenderers", arregloListaTenderers);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(null)
                    .obtenerArregloDocumnetoDocuments();
            documentoTender.append("documents", arregloListaDocuments);

            ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(null)
                    .obtenerArregloDocumnetoMilestones();
            documentoTender.append("milestones", arregloListaMilestones);

            ArrayList<Document> arregloListaAmendments = new ContratacionesListaDocumentoAmendments(null)
                    .obtenerArregloDeAmendments();
            documentoTender.append("amendments", arregloListaAmendments);

            Document documentoAmendment = new ContratacionesDocumentoAmendments(null).obtenerDocumentoAmendments();
            documentoTender.append("amendment", documentoAmendment);

        } else {

            documentoTender.append("id", this.tender.getId());

            documentoTender.append("title", this.tender.getTitle());

            documentoTender.append("description", this.tender.getDescription());

            documentoTender.append("status", this.tender.getStatus());

            Document documentoProcuringEntity = new ContratacionesDocumentoProcuringEntity(
                    this.tender.getProcuringEntity()).obtenerDocumentoProcuringEntity();
            documentoTender.append("procuringEntity", documentoProcuringEntity);

            ArrayList<Document> arregloListaItems = new ContratacionesListaDocumentosItems(this.tender.getItems())
                    .obtenerArregloDeItems();
            documentoTender.append("items", arregloListaItems);

            Document documentoValue = new ContratacionesDocumentoAmount(this.tender.getValue())
                    .obtenerDocumentoAmount();
            documentoTender.append("value", documentoValue);

            Document documentoMinValue = new ContratacionesDocumentoAmount(this.tender.getMinValue())
                    .obtenerDocumentoAmount();
            documentoTender.append("minValue", documentoMinValue);

            documentoTender.append("procurementMethod", this.tender.getProcurementMethod());

            documentoTender.append("procurementMethodDetails", this.tender.getProcurementMethodDetails());

            documentoTender.append("procurementMethodRationale", this.tender.getProcurementMethodRationale());

            documentoTender.append("mainProcurementCategory", this.tender.getMainProcurementCategory());

            ArrayList<String> arregloListAadditionalProcurementCategories = new ContratacionesListaDocumentoAdditionalProcurementCategories(
                    this.tender.getAdditionalProcurementCategories()).obtenerArregloDeAdditionalProcurementCategories();
            documentoTender.append("additionalProcurementCategories", arregloListAadditionalProcurementCategories);

            documentoTender.append("awardCriteria", this.tender.getAwardCriteria());

            documentoTender.append("awardCriteriaDetails", this.tender.getAwardCriteriaDetails());

            ArrayList<String> arregloListAdditionalProcurementCategories = new ContratacionesListaDocumentoSubmissionMethod(
                    this.tender.getSubmissionMethod()).obtenerArregloDeSubmissionMethod();
            documentoTender.append("submissionMethod", arregloListAdditionalProcurementCategories);

            documentoTender.append("submissionMethodDetails", this.tender.getSubmissionMethodDetails());

            Document documentoTenderPeriod = new ContratacionesDocumentoTenderPeriod(this.tender.getTenderPeriod())
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("tenderPeriod", documentoTenderPeriod);

            Document documentoEnquiryPeriod = new ContratacionesDocumentoTenderPeriod(this.tender.getEnquiryPeriod())
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("enquiryPeriod", documentoEnquiryPeriod);

            documentoTender.append("hasEnquiries", this.tender.getHasEnquiries());

            documentoTender.append("eligibilityCriteria", this.tender.getEligibilityCriteria());

            Document documentoAwardPeriod = new ContratacionesDocumentoTenderPeriod(this.tender.getAwardPeriod())
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("awardPeriod", documentoAwardPeriod);

            Document documentoContractPeriod = new ContratacionesDocumentoTenderPeriod(this.tender.getContractPeriod())
                    .obtenerDocumentoTenderPeriod();
            documentoTender.append("contractPeriod", documentoContractPeriod);

            documentoTender.append("numberOfTenderers", this.tender.getNumberOfTenderers());

            ArrayList<Document> arregloListaTenderers = new ContratacionesListaDocumentoTenderers(
                    this.tender.getTenderers()).obtenerArregloDeTenderers();
            documentoTender.append("tenderers", arregloListaTenderers);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(
                    this.tender.getDocuments()).obtenerArregloDocumnetoDocuments();
            documentoTender.append("documents", arregloListaDocuments);

            ArrayList<Document> arregloListaMilestones = new ContratacionesListaDocumentoMilestones(
                    this.tender.getMilestones()).obtenerArregloDocumnetoMilestones();
            documentoTender.append("milestones", arregloListaMilestones);

            ArrayList<Document> arregloListaAmendments = new ContratacionesListaDocumentoAmendments(
                    this.tender.getAmendments()).obtenerArregloDeAmendments();
            documentoTender.append("amendments", arregloListaAmendments);

            Document documentoAmendment = new ContratacionesDocumentoAmendments(this.tender.getAmendment())
                    .obtenerDocumentoAmendments();
            documentoTender.append("amendment", documentoAmendment);

        }

        return documentoTender;

    }

}
