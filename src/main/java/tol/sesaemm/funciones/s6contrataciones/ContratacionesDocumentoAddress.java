/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAddress;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoAddress {

    private ContratacionesAddress address;
    private Document documentoAddress;

    public ContratacionesDocumentoAddress() {
    }

    public ContratacionesDocumentoAddress(ContratacionesAddress address) {
        this.address = address;
    }

    public Document obtenerDocumentoAddress() {
        documentoAddress = new Document();

        if (this.address == null) {

            documentoAddress.append("streetAddress", null);
            documentoAddress.append("locality", null);
            documentoAddress.append("region", null);
            documentoAddress.append("postalCode", null);
            documentoAddress.append("countryName", null);

        } else {
            documentoAddress.append("streetAddress", this.address.getStreetAddress());
            documentoAddress.append("locality", this.address.getLocality());
            documentoAddress.append("region", this.address.getRegion());
            documentoAddress.append("postalCode", this.address.getPostalCode());
            documentoAddress.append("countryName", this.address.getCountryName());

        }

        return documentoAddress;
    }

}