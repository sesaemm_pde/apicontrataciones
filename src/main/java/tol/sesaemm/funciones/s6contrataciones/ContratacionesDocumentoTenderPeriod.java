/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTenderPeriod;

/**
 *
 * @author Ismael OrtIsmaeliz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoTenderPeriod {

    private ContratacionesTenderPeriod tenderPeriod;
    private Document documentoTenderPeriod;

    public ContratacionesDocumentoTenderPeriod() {
    }

    public ContratacionesDocumentoTenderPeriod(ContratacionesTenderPeriod tenderPeriod) {
        this.tenderPeriod = tenderPeriod;
    }

    public Document obtenerDocumentoTenderPeriod() {
        documentoTenderPeriod = new Document();

        if (this.tenderPeriod == null) {

            documentoTenderPeriod.append("startDate", null);

            documentoTenderPeriod.append("endDate", null);

            documentoTenderPeriod.append("maxExtentDate", null);

            documentoTenderPeriod.append("durationInDays", null);

        } else {

            documentoTenderPeriod.append("startDate", this.tenderPeriod.getStartDate());

            documentoTenderPeriod.append("endDate", this.tenderPeriod.getEndDate());

            documentoTenderPeriod.append("maxExtentDate", this.tenderPeriod.getMaxExtentDate());

            documentoTenderPeriod.append("durationInDays", this.tenderPeriod.getDurationInDays());

        }

        return documentoTenderPeriod;

    }

}
