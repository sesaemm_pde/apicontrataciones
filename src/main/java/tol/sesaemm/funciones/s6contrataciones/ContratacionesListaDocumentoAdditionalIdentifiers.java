/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesIdentifier;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoAdditionalIdentifiers {

    private ArrayList<ContratacionesIdentifier> identifier;
    private ArrayList<Document> arregloListaIdentifier;

    public ContratacionesListaDocumentoAdditionalIdentifiers() {
    }

    public ContratacionesListaDocumentoAdditionalIdentifiers(ArrayList<ContratacionesIdentifier> identifier) {
        this.identifier = identifier;
    }

    public ArrayList<Document> obtenerArregloDocumnetosIdentifiers() {
        arregloListaIdentifier = new ArrayList<>();

        if (this.identifier == null || this.identifier.isEmpty()) {
            Document documentoIdentifier = new Document();

            documentoIdentifier.append("scheme", null);

            documentoIdentifier.append("id", null);

            documentoIdentifier.append("legalName", null);

            documentoIdentifier.append("uri", null);

            arregloListaIdentifier.add(documentoIdentifier);
        } else {
            for (int i = 0; i < this.identifier.size(); i++) {

                Document documentoIdentifier = new Document();

                documentoIdentifier.append("scheme", this.identifier.get(i).getScheme());

                documentoIdentifier.append("id", this.identifier.get(i).getId());

                documentoIdentifier.append("legalName", this.identifier.get(i).getLegalName());

                documentoIdentifier.append("uri", this.identifier.get(i).getUri());

                arregloListaIdentifier.add(documentoIdentifier);

            }
        }

        return arregloListaIdentifier;
    }

}
