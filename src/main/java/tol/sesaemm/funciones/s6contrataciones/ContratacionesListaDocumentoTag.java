/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoTag {

    private ArrayList<String> tags;
    private ArrayList<String> arregloListaTags;

    public ContratacionesListaDocumentoTag() {
    }

    public ContratacionesListaDocumentoTag(ArrayList<String> tags) {
        this.tags = tags;
    }

    public ArrayList<String> obtenerArregloDeTag() {

        arregloListaTags = new ArrayList<>();

        if (this.tags == null || this.tags.isEmpty()) {
            arregloListaTags.add(null);
        } else {
            for (int i = 0; i < this.tags.size(); i++) {
                if (this.tags.get(i) == null) {
                    arregloListaTags.add(null);
                } else {
                    arregloListaTags.add(this.tags.get(i));
                }
            }
        }

        return arregloListaTags;

    }

}
