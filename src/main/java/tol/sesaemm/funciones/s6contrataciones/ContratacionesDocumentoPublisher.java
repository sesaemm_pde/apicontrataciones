/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesPublisher;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoPublisher {

    private ContratacionesPublisher publisher;
    private Document documentoPublisher;

    public ContratacionesDocumentoPublisher() {
    }

    public ContratacionesDocumentoPublisher(ContratacionesPublisher publisher) {
        this.publisher = publisher;
    }

    public Document obtenerDocumentoPublisher() {
        documentoPublisher = new Document();

        if (this.publisher == null) {

            documentoPublisher.append("name", null);

            documentoPublisher.append("scheme", null);

            documentoPublisher.append("uid", null);

            documentoPublisher.append("uri", null);

        } else {

            documentoPublisher.append("name", this.publisher.getName());

            documentoPublisher.append("scheme", this.publisher.getScheme());

            documentoPublisher.append("uid", this.publisher.getUid());

            documentoPublisher.append("uri", this.publisher.getUri());

        }

        return documentoPublisher;

    }

}
