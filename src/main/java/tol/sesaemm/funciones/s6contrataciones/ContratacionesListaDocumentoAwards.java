/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAwards;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoAwards {

    private ArrayList<ContratacionesAwards> awards;
    private ArrayList<Document> arregloListaAwards;

    public ContratacionesListaDocumentoAwards() {
    }

    public ContratacionesListaDocumentoAwards(ArrayList<ContratacionesAwards> awards) {
        this.awards = awards;
    }

    public ArrayList<Document> obtenerArregloDeAwards() {
        arregloListaAwards = new ArrayList<>();

        if (this.awards == null || this.awards.isEmpty() == true) {
            Document documentoAward = new Document();

            documentoAward.append("id", null);

            documentoAward.append("title", null);

            documentoAward.append("description", null);

            documentoAward.append("status", null);

            documentoAward.append("date", null);

            Document documentoValue = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoAward.append("value", documentoValue);

            ArrayList<Document> arregloListaSuppliers = new ContratacionesListaDocumentoTenderers(null)
                    .obtenerArregloDeTenderers();
            documentoAward.append("suppliers", arregloListaSuppliers);

            ArrayList<Document> arregloListaItems = new ContratacionesListaDocumentosItems(null)
                    .obtenerArregloDeItems();
            documentoAward.append("items", arregloListaItems);

            Document documentoContractPeriod = new ContratacionesDocumentoTenderPeriod(null)
                    .obtenerDocumentoTenderPeriod();
            documentoAward.append("contractPeriod", documentoContractPeriod);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(null)
                    .obtenerArregloDocumnetoDocuments();
            documentoAward.append("documents", arregloListaDocuments);

            ArrayList<Document> arregloListaAmendments = new ContratacionesListaDocumentoAmendments(null)
                    .obtenerArregloDeAmendments();
            documentoAward.append("amendments", arregloListaAmendments);

            Document documentoAmendment = new ContratacionesDocumentoAmendments(null).obtenerDocumentoAmendments();
            documentoAward.append("amendment", documentoAmendment);

            arregloListaAwards.add(documentoAward);
        } else {

            for (int i = 0; i < this.awards.size(); i++) {
                Document documentoAward = new Document();

                documentoAward.append("id", this.awards.get(i).getId());

                documentoAward.append("title", this.awards.get(i).getTitle());

                documentoAward.append("description", this.awards.get(i).getDescription());

                documentoAward.append("status", this.awards.get(i).getStatus());

                documentoAward.append("date", this.awards.get(i).getDate());

                Document documentoValue = new ContratacionesDocumentoAmount(this.awards.get(i).getValue())
                        .obtenerDocumentoAmount();
                documentoAward.append("value", documentoValue);

                ArrayList<Document> arregloListaSuppliers = new ContratacionesListaDocumentoTenderers(
                        this.awards.get(i).getSuppliers()).obtenerArregloDeTenderers();
                documentoAward.append("suppliers", arregloListaSuppliers);

                ArrayList<Document> arregloListaItems = new ContratacionesListaDocumentosItems(
                        this.awards.get(i).getItems()).obtenerArregloDeItems();
                documentoAward.append("items", arregloListaItems);

                Document documentoContractPeriod = new ContratacionesDocumentoTenderPeriod(
                        this.awards.get(i).getContractPeriod()).obtenerDocumentoTenderPeriod();
                documentoAward.append("contractPeriod", documentoContractPeriod);

                ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(
                        this.awards.get(i).getDocuments()).obtenerArregloDocumnetoDocuments();
                documentoAward.append("documents", arregloListaDocuments);

                ArrayList<Document> arregloListaAmendments = new ContratacionesListaDocumentoAmendments(
                        this.awards.get(i).getAmendments()).obtenerArregloDeAmendments();
                documentoAward.append("amendments", arregloListaAmendments);

                Document documentoAmendment = new ContratacionesDocumentoAmendments(this.awards.get(i).getAmendment())
                        .obtenerDocumentoAmendments();
                documentoAward.append("amendment", documentoAmendment);

                arregloListaAwards.add(documentoAward);

            }

        }

        return arregloListaAwards;

    }

}
