/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTenderers;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoTenderers {

    private ContratacionesTenderers tenderers;
    private Document documentoTenderers;

    public ContratacionesDocumentoTenderers() {
    }

    public ContratacionesDocumentoTenderers(ContratacionesTenderers tenderers) {
        this.tenderers = tenderers;
    }

    public Document obtenerDocumentoTenderers() {
        documentoTenderers = new Document();

        if (this.tenderers == null) {

            documentoTenderers.append("name", null);

            documentoTenderers.append("id", null);

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(null).obtenerDocumentoIdentifier();
            documentoTenderers.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    null).obtenerArregloDocumnetosIdentifiers();
            documentoTenderers.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(null)
                    .obtenerDocumentoContactPoint();
            documentoTenderers.append("contactPoint", documentoContactPoint);

            Document documentoAddress = new ContratacionesDocumentoAddress(null).obtenerDocumentoAddress();
            documentoTenderers.append("address", documentoAddress);

        } else {

            documentoTenderers.append("name", this.tenderers.getName());

            documentoTenderers.append("id", this.tenderers.getId());

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(this.tenderers.getIdentifier())
                    .obtenerDocumentoIdentifier();
            documentoTenderers.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    this.tenderers.getAdditionalIdentifiers()).obtenerArregloDocumnetosIdentifiers();
            documentoTenderers.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(this.tenderers.getContactPoint())
                    .obtenerDocumentoContactPoint();
            documentoTenderers.append("contactPoint", documentoContactPoint);

            Document documentoAddress = new ContratacionesDocumentoAddress(this.tenderers.getAddress())
                    .obtenerDocumentoAddress();
            documentoTenderers.append("address", documentoAddress);

        }

        return documentoTenderers;

    }

}
