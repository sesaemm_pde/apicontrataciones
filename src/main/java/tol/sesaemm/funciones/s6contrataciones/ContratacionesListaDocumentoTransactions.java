/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTransactions;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoTransactions {

    private ArrayList<ContratacionesTransactions> transactions;
    private ArrayList<Document> arregloListaTransactions;

    public ContratacionesListaDocumentoTransactions() {
    }

    public ContratacionesListaDocumentoTransactions(ArrayList<ContratacionesTransactions> transactions) {
        this.transactions = transactions;
    }

    public ArrayList<Document> obtenerArregloDeTransactions() {
        arregloListaTransactions = new ArrayList<>();

        if (transactions == null || transactions.isEmpty() == true) {
            Document documentoTransaction = new Document();

            documentoTransaction.append("id", null);

            documentoTransaction.append("source", null);

            documentoTransaction.append("date", null);

            Document documentoValue = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoTransaction.append("value", documentoValue);

            Document documentoPayer = new ContratacionesDocumentoTenderers(null).obtenerDocumentoTenderers();
            documentoTransaction.append("payer", documentoPayer);

            Document documentoPayee = new ContratacionesDocumentoTenderers(null).obtenerDocumentoTenderers();
            documentoTransaction.append("payee", documentoPayee);

            documentoTransaction.append("uri", null);

            Document documentoAmount = new ContratacionesDocumentoAmount(null).obtenerDocumentoAmount();
            documentoTransaction.append("amount", documentoAmount);

            Document documentoProviderOrganization = new ContratacionesDocumentoIdentifier(null)
                    .obtenerDocumentoIdentifier();
            documentoTransaction.append("providerOrganization", documentoProviderOrganization);

            Document documentoReceiverOrganization = new ContratacionesDocumentoIdentifier(null)
                    .obtenerDocumentoIdentifier();
            documentoTransaction.append("receiverOrganization", documentoReceiverOrganization);

            arregloListaTransactions.add(documentoTransaction);
        } else {

            for (int i = 0; i < transactions.size(); i++) {
                Document documentoTransaction = new Document();

                documentoTransaction.append("id", transactions.get(i).getId());

                documentoTransaction.append("source", transactions.get(i).getSource());

                documentoTransaction.append("date", transactions.get(i).getDate());

                Document documentoValue = new ContratacionesDocumentoAmount(transactions.get(i).getValue())
                        .obtenerDocumentoAmount();
                documentoTransaction.append("value", documentoValue);

                Document documentoPayer = new ContratacionesDocumentoTenderers(transactions.get(i).getPayer())
                        .obtenerDocumentoTenderers();
                documentoTransaction.append("payer", documentoPayer);

                Document documentoPayee = new ContratacionesDocumentoTenderers(transactions.get(i).getPayee())
                        .obtenerDocumentoTenderers();
                documentoTransaction.append("payee", documentoPayee);

                documentoTransaction.append("uri", transactions.get(i).getUri());

                Document documentoAmount = new ContratacionesDocumentoAmount(this.transactions.get(i).getAmount())
                        .obtenerDocumentoAmount();
                documentoTransaction.append("amount", documentoAmount);

                Document documentoProviderOrganization = new ContratacionesDocumentoIdentifier(
                        this.transactions.get(i).getProviderOrganization()).obtenerDocumentoIdentifier();
                documentoTransaction.append("providerOrganization", documentoProviderOrganization);

                Document documentoReceiverOrganization = new ContratacionesDocumentoIdentifier(
                        this.transactions.get(i).getReceiverOrganization()).obtenerDocumentoIdentifier();
                documentoTransaction.append("receiverOrganization", documentoReceiverOrganization);

                arregloListaTransactions.add(documentoTransaction);

            }

        }

        return arregloListaTransactions;

    }

}
