/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesMilestones;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoMilestones {

    private ArrayList<ContratacionesMilestones> milestones;
    private ArrayList<Document> arregloListaMilestones;

    public ContratacionesListaDocumentoMilestones() {
    }

    public ContratacionesListaDocumentoMilestones(ArrayList<ContratacionesMilestones> milestones) {
        this.milestones = milestones;

    }

    public ArrayList<Document> obtenerArregloDocumnetoMilestones() {
        arregloListaMilestones = new ArrayList<>();

        if (milestones == null || milestones.isEmpty() == true) {
            Document documentMilestones = new Document();

            documentMilestones.append("id", null);

            documentMilestones.append("title", null);

            documentMilestones.append("type", null);

            documentMilestones.append("description", null);

            documentMilestones.append("code", null);

            documentMilestones.append("dueDate", null);

            documentMilestones.append("dateMet", null);

            documentMilestones.append("dateModified", null);

            documentMilestones.append("status", null);

            ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(null)
                    .obtenerArregloDocumnetoDocuments();
            documentMilestones.append("documents", arregloListaDocuments);

            arregloListaMilestones.add(documentMilestones);
        } else {

            for (int i = 0; i < milestones.size(); i++) {
                Document documentMilestones = new Document();

                documentMilestones.append("id", this.milestones.get(i).getId());

                documentMilestones.append("title", this.milestones.get(i).getTitle());

                documentMilestones.append("type", this.milestones.get(i).getType());

                documentMilestones.append("description", this.milestones.get(i).getDescription());

                documentMilestones.append("code", this.milestones.get(i).getCode());

                documentMilestones.append("dueDate", this.milestones.get(i).getDueDate());

                documentMilestones.append("dateMet", this.milestones.get(i).getDateMet());

                documentMilestones.append("dateModified", this.milestones.get(i).getDateModified());

                documentMilestones.append("status", this.milestones.get(i).getStatus());

                ArrayList<Document> arregloListaDocuments = new ContratacionesListaDocumentoDocuments(
                        this.milestones.get(i).getDocuments()).obtenerArregloDocumnetoDocuments();
                documentMilestones.append("documents", arregloListaDocuments);

                arregloListaMilestones.add(documentMilestones);

            }

        }

        return arregloListaMilestones;
    }

}
