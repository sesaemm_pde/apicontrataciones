/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesRelatedProcesses;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesListaDocumentoRelatedProcesses {

    private ArrayList<ContratacionesRelatedProcesses> relatedProcesses;
    private ArrayList<Document> arregloListaRelatedProcesses;

    public ContratacionesListaDocumentoRelatedProcesses() {
    }

    public ContratacionesListaDocumentoRelatedProcesses(ArrayList<ContratacionesRelatedProcesses> relatedProcesses) {
        this.relatedProcesses = relatedProcesses;
    }

    public ArrayList<Document> obtenerArregloDeRelatedProcesses() {
        arregloListaRelatedProcesses = new ArrayList<>();

        if (this.relatedProcesses == null || this.relatedProcesses.isEmpty() == true) {
            Document documentoRelatedProcesses = new Document();

            documentoRelatedProcesses.append("id", null);

            ArrayList<String> arregloListaRelationship = new ContratacionesListaDocumentoRelationship(null)
                    .obtenerArregloDeRelationship();
            documentoRelatedProcesses.append("relationship", arregloListaRelationship);

            documentoRelatedProcesses.append("title", null);

            documentoRelatedProcesses.append("scheme", null);

            documentoRelatedProcesses.append("identifier", null);

            documentoRelatedProcesses.append("uri", null);

            arregloListaRelatedProcesses.add(documentoRelatedProcesses);
        } else {

            for (int i = 0; i < this.relatedProcesses.size(); i++) {
                Document documentoRelatedProcesses = new Document();

                documentoRelatedProcesses.append("id", this.relatedProcesses.get(i).getId());

                ArrayList<String> arregloListaRelationship = new ContratacionesListaDocumentoRelationship(
                        this.relatedProcesses.get(i).getRelationship()).obtenerArregloDeRelationship();
                documentoRelatedProcesses.append("relationship", arregloListaRelationship);

                documentoRelatedProcesses.append("title", this.relatedProcesses.get(i).getTitle());

                documentoRelatedProcesses.append("scheme", this.relatedProcesses.get(i).getScheme());

                documentoRelatedProcesses.append("identifier", this.relatedProcesses.get(i).getIdentifier());

                documentoRelatedProcesses.append("uri", this.relatedProcesses.get(i).getUri());

                arregloListaRelatedProcesses.add(documentoRelatedProcesses);

            }

        }

        return arregloListaRelatedProcesses;

    }

}
