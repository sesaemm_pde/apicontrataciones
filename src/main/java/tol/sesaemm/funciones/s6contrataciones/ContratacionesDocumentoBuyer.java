/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s6contrataciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesBuyer;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesDocumentoBuyer {

    private ContratacionesBuyer buyer;
    private Document documentoBuyer;

    public ContratacionesDocumentoBuyer() {
    }

    public ContratacionesDocumentoBuyer(ContratacionesBuyer buyer) {
        this.buyer = buyer;
    }

    public Document obtenerDocumentoBuyer() {
        documentoBuyer = new Document();

        if (this.buyer == null) {

            documentoBuyer.append("name", null);

            documentoBuyer.append("id", null);

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(null).obtenerDocumentoIdentifier();
            documentoBuyer.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    null).obtenerArregloDocumnetosIdentifiers();
            documentoBuyer.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(null)
                    .obtenerDocumentoContactPoint();
            documentoBuyer.append("contactPoint", documentoContactPoint);

            Document documentoAddress = new ContratacionesDocumentoAddress(null).obtenerDocumentoAddress();
            documentoBuyer.append("address", documentoAddress);

        } else {

            documentoBuyer.append("name", this.buyer.getName());

            documentoBuyer.append("id", this.buyer.getId());

            Document documentoIdentifier = new ContratacionesDocumentoIdentifier(this.buyer.getIdentifier())
                    .obtenerDocumentoIdentifier();
            documentoBuyer.append("identifier", documentoIdentifier);

            ArrayList<Document> arregloListaAdditionalIdentifiers = new ContratacionesListaDocumentoAdditionalIdentifiers(
                    this.buyer.getAdditionalIdentifiers()).obtenerArregloDocumnetosIdentifiers();
            documentoBuyer.append("additionalIdentifiers", arregloListaAdditionalIdentifiers);

            Document documentoContactPoint = new ContratacionesDocumentoContactPoint(this.buyer.getContactPoint())
                    .obtenerDocumentoContactPoint();
            documentoBuyer.append("contactPoint", documentoContactPoint);

            Document documentoAddress = new ContratacionesDocumentoAddress(this.buyer.getAddress())
                    .obtenerDocumentoAddress();
            documentoBuyer.append("address", documentoAddress);

        }

        return documentoBuyer;

    }

}
