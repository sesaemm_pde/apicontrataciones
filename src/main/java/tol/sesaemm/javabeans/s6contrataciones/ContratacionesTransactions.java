/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesTransactions implements Serializable
{

    private Object id;
    private String source;
    private String date;
    private ContratacionesAmount value;
    private ContratacionesTenderers payer;
    private ContratacionesTenderers payee;
    private String uri;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private ContratacionesAmount amount;
    private ContratacionesIdentifier providerOrganization;
    private ContratacionesIdentifier receiverOrganization;
    //</editor-fold>
    
    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public ContratacionesAmount getValue()
    {
        return value;
    }

    public void setValue(ContratacionesAmount value)
    {
        this.value = value;
    }

    public ContratacionesTenderers getPayer()
    {
        return payer;
    }

    public void setPayer(ContratacionesTenderers payer)
    {
        this.payer = payer;
    }

    public ContratacionesTenderers getPayee()
    {
        return payee;
    }

    public void setPayee(ContratacionesTenderers payee)
    {
        this.payee = payee;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public ContratacionesAmount getAmount()
    {
        return amount;
    }

    public void setAmount(ContratacionesAmount amount)
    {
        this.amount = amount;
    }

    public ContratacionesIdentifier getProviderOrganization()
    {
        return providerOrganization;
    }

    public void setProviderOrganization(ContratacionesIdentifier providerOrganization)
    {
        this.providerOrganization = providerOrganization;
    }

    public ContratacionesIdentifier getReceiverOrganization()
    {
        return receiverOrganization;
    }

    public void setReceiverOrganization(ContratacionesIdentifier receiverOrganization)
    {
        this.receiverOrganization = receiverOrganization;
    }

}