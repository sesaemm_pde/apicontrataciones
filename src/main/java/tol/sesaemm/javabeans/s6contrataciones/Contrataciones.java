/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;
import tol.sesaemm.javabeans.PAGINATION;

/**
 *
 * @author Ismael Ortz ismael.ortiz@sesaemm.org.mx
 */
public class Contrataciones implements Serializable
{

    private PAGINATION pagination;
    private ArrayList<ContratacionesContratacion> results;

    public PAGINATION getPagination()
    {
        return pagination;
    }

    public void setPagination(PAGINATION pagination)
    {
        this.pagination = pagination;
    }

    public ArrayList<ContratacionesContratacion> getResults()
    {
        return results;
    }

    public void setResults(ArrayList<ContratacionesContratacion> results)
    {
        this.results = results;
    }

}
