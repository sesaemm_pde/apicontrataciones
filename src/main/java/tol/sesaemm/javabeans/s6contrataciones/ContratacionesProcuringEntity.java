/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesProcuringEntity implements Serializable
{

    private String name;
    private Object id;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private ContratacionesIdentifier identifier;
    private ArrayList<ContratacionesIdentifier> additionalIdentifiers;
    private ContratacionesAddress address;
    private ContratacionesContactPoint contactPoint;
    //</editor-fold>
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public ContratacionesIdentifier getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(ContratacionesIdentifier identifier)
    {
        this.identifier = identifier;
    }

    public ArrayList<ContratacionesIdentifier> getAdditionalIdentifiers()
    {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(ArrayList<ContratacionesIdentifier> additionalIdentifiers)
    {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public ContratacionesAddress getAddress()
    {
        return address;
    }

    public void setAddress(ContratacionesAddress address)
    {
        this.address = address;
    }

    public ContratacionesContactPoint getContactPoint()
    {
        return contactPoint;
    }

    public void setContactPoint(ContratacionesContactPoint contactPoint)
    {
        this.contactPoint = contactPoint;
    }

}