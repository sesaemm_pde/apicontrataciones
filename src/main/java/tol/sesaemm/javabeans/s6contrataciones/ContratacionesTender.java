/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesTender implements Serializable
{

    private Object id;
    private String title;
    private String description;
    private String status;
    private ContratacionesProcuringEntity procuringEntity;
    private ArrayList<ContratacionesItems> items;
    private ContratacionesAmount value;
    private ContratacionesAmount minValue;
    private String procurementMethod;
    private String procurementMethodDetails;
    private String procurementMethodRationale;
    private String mainProcurementCategory;
    private ArrayList<String> additionalProcurementCategories;
    private String awardCriteria;
    private String awardCriteriaDetails;
    private ArrayList<String> submissionMethod;
    private String submissionMethodDetails;
    private ContratacionesTenderPeriod tenderPeriod;
    private ContratacionesTenderPeriod enquiryPeriod;
    private Boolean hasEnquiries;
    private String eligibilityCriteria;
    private ContratacionesTenderPeriod awardPeriod;
    private ContratacionesTenderPeriod contractPeriod;
    private Integer numberOfTenderers;
    private ArrayList<ContratacionesTenderers> tenderers;
    private ArrayList<ContratacionesDocuments> documents;
    private ArrayList<ContratacionesMilestones> milestones;
    private ArrayList<ContratacionesAmendments> amendments;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private ContratacionesAmendments amendment;
    //</editor-fold>
    
    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public ContratacionesProcuringEntity getProcuringEntity()
    {
        return procuringEntity;
    }

    public void setProcuringEntity(ContratacionesProcuringEntity procuringEntity)
    {
        this.procuringEntity = procuringEntity;
    }

    public ArrayList<ContratacionesItems> getItems()
    {
        return items;
    }

    public void setItems(ArrayList<ContratacionesItems> items)
    {
        this.items = items;
    }

    public ContratacionesAmount getValue()
    {
        return value;
    }

    public void setValue(ContratacionesAmount value)
    {
        this.value = value;
    }

    public ContratacionesAmount getMinValue()
    {
        return minValue;
    }

    public void setMinValue(ContratacionesAmount minValue)
    {
        this.minValue = minValue;
    }

    public String getProcurementMethod()
    {
        return procurementMethod;
    }

    public void setProcurementMethod(String procurementMethod)
    {
        this.procurementMethod = procurementMethod;
    }

    public String getProcurementMethodDetails()
    {
        return procurementMethodDetails;
    }

    public void setProcurementMethodDetails(String procurementMethodDetails)
    {
        this.procurementMethodDetails = procurementMethodDetails;
    }

    public String getProcurementMethodRationale()
    {
        return procurementMethodRationale;
    }

    public void setProcurementMethodRationale(String procurementMethodRationale)
    {
        this.procurementMethodRationale = procurementMethodRationale;
    }

    public String getMainProcurementCategory()
    {
        return mainProcurementCategory;
    }

    public void setMainProcurementCategory(String mainProcurementCategory)
    {
        this.mainProcurementCategory = mainProcurementCategory;
    }

    public ArrayList<String> getAdditionalProcurementCategories()
    {
        return additionalProcurementCategories;
    }

    public void setAdditionalProcurementCategories(ArrayList<String> additionalProcurementCategories)
    {
        this.additionalProcurementCategories = additionalProcurementCategories;
    }

    public String getAwardCriteria()
    {
        return awardCriteria;
    }

    public void setAwardCriteria(String awardCriteria)
    {
        this.awardCriteria = awardCriteria;
    }

    public String getAwardCriteriaDetails()
    {
        return awardCriteriaDetails;
    }

    public void setAwardCriteriaDetails(String awardCriteriaDetails)
    {
        this.awardCriteriaDetails = awardCriteriaDetails;
    }

    public ArrayList<String> getSubmissionMethod()
    {
        return submissionMethod;
    }

    public void setSubmissionMethod(ArrayList<String> submissionMethod)
    {
        this.submissionMethod = submissionMethod;
    }

    public String getSubmissionMethodDetails()
    {
        return submissionMethodDetails;
    }

    public void setSubmissionMethodDetails(String submissionMethodDetails)
    {
        this.submissionMethodDetails = submissionMethodDetails;
    }

    public ContratacionesTenderPeriod getTenderPeriod()
    {
        return tenderPeriod;
    }

    public void setTenderPeriod(ContratacionesTenderPeriod tenderPeriod)
    {
        this.tenderPeriod = tenderPeriod;
    }

    public ContratacionesTenderPeriod getEnquiryPeriod()
    {
        return enquiryPeriod;
    }

    public void setEnquiryPeriod(ContratacionesTenderPeriod enquiryPeriod)
    {
        this.enquiryPeriod = enquiryPeriod;
    }

    public Boolean getHasEnquiries()
    {
        return hasEnquiries;
    }

    public void setHasEnquiries(Boolean hasEnquiries)
    {
        this.hasEnquiries = hasEnquiries;
    }

    public String getEligibilityCriteria()
    {
        return eligibilityCriteria;
    }

    public void setEligibilityCriteria(String eligibilityCriteria)
    {
        this.eligibilityCriteria = eligibilityCriteria;
    }

    public ContratacionesTenderPeriod getAwardPeriod()
    {
        return awardPeriod;
    }

    public void setAwardPeriod(ContratacionesTenderPeriod awardPeriod)
    {
        this.awardPeriod = awardPeriod;
    }

    public ContratacionesTenderPeriod getContractPeriod()
    {
        return contractPeriod;
    }

    public void setContractPeriod(ContratacionesTenderPeriod contractPeriod)
    {
        this.contractPeriod = contractPeriod;
    }

    public Integer getNumberOfTenderers()
    {
        return numberOfTenderers;
    }

    public void setNumberOfTenderers(Integer numberOfTenderers)
    {
        this.numberOfTenderers = numberOfTenderers;
    }

    public ArrayList<ContratacionesTenderers> getTenderers()
    {
        return tenderers;
    }

    public void setTenderers(ArrayList<ContratacionesTenderers> tenderers)
    {
        this.tenderers = tenderers;
    }

    public ArrayList<ContratacionesDocuments> getDocuments()
    {
        return documents;
    }

    public void setDocuments(ArrayList<ContratacionesDocuments> documents)
    {
        this.documents = documents;
    }

    public ArrayList<ContratacionesMilestones> getMilestones()
    {
        return milestones;
    }

    public void setMilestones(ArrayList<ContratacionesMilestones> milestones)
    {
        this.milestones = milestones;
    }

    public ArrayList<ContratacionesAmendments> getAmendments()
    {
        return amendments;
    }

    public void setAmendments(ArrayList<ContratacionesAmendments> amendments)
    {
        this.amendments = amendments;
    }

    public ContratacionesAmendments getAmendment()
    {
        return amendment;
    }

    public void setAmendment(ContratacionesAmendments amendment)
    {
        this.amendment = amendment;
    }

}