/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesMilestones implements Serializable
{

    private Object id;
    private String title;
    private String type;
    private String description;
    private String code;
    private String dueDate;
    private String dateMet;
    private String dateModified;
    private String status;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private ArrayList<ContratacionesDocuments> documents;
    //</editor-fold>
    
    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDueDate()
    {
        return dueDate;
    }

    public void setDueDate(String dueDate)
    {
        this.dueDate = dueDate;
    }

    public String getDateMet()
    {
        return dateMet;
    }

    public void setDateMet(String dateMet)
    {
        this.dateMet = dateMet;
    }

    public String getDateModified()
    {
        return dateModified;
    }

    public void setDateModified(String dateModified)
    {
        this.dateModified = dateModified;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public ArrayList<ContratacionesDocuments> getDocuments()
    {
        return documents;
    }

    public void setDocuments(ArrayList<ContratacionesDocuments> documents)
    {
        this.documents = documents;
    }

}