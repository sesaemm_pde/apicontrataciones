/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesImplementation implements Serializable
{

    private ArrayList<ContratacionesTransactions> transactions;
    private ArrayList<ContratacionesMilestones> milestones;
    private ArrayList<ContratacionesDocuments> documents;

    public ArrayList<ContratacionesTransactions> getTransactions()
    {
        return transactions;
    }

    public void setTransactions(ArrayList<ContratacionesTransactions> transactions)
    {
        this.transactions = transactions;
    }

    public ArrayList<ContratacionesMilestones> getMilestones()
    {
        return milestones;
    }

    public void setMilestones(ArrayList<ContratacionesMilestones> milestones)
    {
        this.milestones = milestones;
    }

    public ArrayList<ContratacionesDocuments> getDocuments()
    {
        return documents;
    }

    public void setDocuments(ArrayList<ContratacionesDocuments> documents)
    {
        this.documents = documents;
    }

}
