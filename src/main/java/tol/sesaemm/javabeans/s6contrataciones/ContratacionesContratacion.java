/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import org.bson.codecs.pojo.annotations.BsonProperty;
import tol.sesaemm.javabeans.METADATA;
import tol.sesaemm.javabeans.METADATOS;

/**
 *
 * @author Ismael Ortz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesContratacion implements Serializable
{

    private METADATA metadata;
    private ContratacionesPublisher publisher;
    private Integer cycle;
    private String ocid;
    @BsonProperty("id")
    private String id;
    private String date;
    private ArrayList<String> tag;
    private String initiationType;
    private ArrayList<ContratacionesParties> parties;
    private ContratacionesBuyer buyer;
    private ContratacionesPlanning planning;
    private ContratacionesTender tender;
    private String language;
    private ArrayList<ContratacionesAwards> awards;
    private ArrayList<ContratacionesContracts> contracts;
    //<editor-fold defaultstate="collapsed" desc="campos utilizados en la PDE">
    @JsonIgnore
    private String publicar;
    @JsonIgnore
    private METADATOS metadatos;
    @JsonIgnore
    private String dependencia;
    //<editor-fold defaultstate="collapsed" desc="campos utilizados para identificar el estado de las secciones de una contratacion">
    @JsonIgnore                                                                 // anotacion para jackson
    private String estado_registro;
    @JsonIgnore                                                                 // anotacion para jackson
    private ArrayList<ContratacionesEstadoSeccion> estado_secciones_contratacion;
    //</editor-fold>
    //</editor-fold>

    public METADATA getMetadata()
    {
        return metadata;
    }

    public void setMetadata(METADATA metadata)
    {
        this.metadata = metadata;
    }

    public ContratacionesPublisher getPublisher()
    {
        return publisher;
    }

    public void setPublisher(ContratacionesPublisher publisher)
    {
        this.publisher = publisher;
    }

    public Integer getCycle()
    {
        return cycle;
    }

    public void setCycle(Integer cycle)
    {
        this.cycle = cycle;
    }

    public String getOcid()
    {
        return ocid;
    }

    public void setOcid(String ocid)
    {
        this.ocid = ocid;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public ArrayList<String> getTag()
    {
        return tag;
    }

    public void setTag(ArrayList<String> tag)
    {
        this.tag = tag;
    }

    public String getInitiationType()
    {
        return initiationType;
    }

    public void setInitiationType(String initiationType)
    {
        this.initiationType = initiationType;
    }

    public ArrayList<ContratacionesParties> getParties()
    {
        return parties;
    }

    public void setParties(ArrayList<ContratacionesParties> parties)
    {
        this.parties = parties;
    }

    public ContratacionesBuyer getBuyer()
    {
        return buyer;
    }

    public void setBuyer(ContratacionesBuyer buyer)
    {
        this.buyer = buyer;
    }

    public ContratacionesPlanning getPlanning()
    {
        return planning;
    }

    public void setPlanning(ContratacionesPlanning planning)
    {
        this.planning = planning;
    }

    public ContratacionesTender getTender()
    {
        return tender;
    }

    public void setTender(ContratacionesTender tender)
    {
        this.tender = tender;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public ArrayList<ContratacionesAwards> getAwards()
    {
        return awards;
    }

    public void setAwards(ArrayList<ContratacionesAwards> awards)
    {
        this.awards = awards;
    }

    public ArrayList<ContratacionesContracts> getContracts()
    {
        return contracts;
    }

    public void setContracts(ArrayList<ContratacionesContracts> contracts)
    {
        this.contracts = contracts;
    }

    public String getPublicar()
    {
        return publicar;
    }

    public void setPublicar(String publicar)
    {
        this.publicar = publicar;
    }

    public METADATOS getMetadatos()
    {
        return metadatos;
    }

    public void setMetadatos(METADATOS metadatos)
    {
        this.metadatos = metadatos;
    }

    public String getDependencia()
    {
        return dependencia;
    }

    public void setDependencia(String dependencia)
    {
        this.dependencia = dependencia;
    }

    public String getEstado_registro()
    {
        return estado_registro;
    }

    public void setEstado_registro(String estado_registro)
    {
        this.estado_registro = estado_registro;
    }

    public ArrayList<ContratacionesEstadoSeccion> getEstado_secciones_contratacion()
    {
        return estado_secciones_contratacion;
    }

    public void setEstado_secciones_contratacion(ArrayList<ContratacionesEstadoSeccion> estado_secciones_contratacion)
    {
        this.estado_secciones_contratacion = estado_secciones_contratacion;
    }

}