/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesContracts implements Serializable
{

    private Object id;
    private Object awardID;
    private String title;
    private String description;
    private String status;
    private ContratacionesTenderPeriod period;
    private ContratacionesAmount value;
    private ArrayList<ContratacionesItems> items;
    private String dateSigned;
    private ArrayList<ContratacionesDocuments> documents;
    private ContratacionesImplementation implementation;
    private ArrayList<ContratacionesRelatedProcesses> relatedProcesses;
    private ArrayList<ContratacionesMilestones> milestones;
    private ArrayList<ContratacionesAmendments> amendments;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private ContratacionesAmendments amendment;
    //</editor-fold>
    
    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public Object getAwardID()
    {
        return awardID;
    }

    public void setAwardID(Object awardID)
    {
        this.awardID = awardID;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public ContratacionesTenderPeriod getPeriod()
    {
        return period;
    }

    public void setPeriod(ContratacionesTenderPeriod period)
    {
        this.period = period;
    }

    public ContratacionesAmount getValue()
    {
        return value;
    }

    public void setValue(ContratacionesAmount value)
    {
        this.value = value;
    }

    public ArrayList<ContratacionesItems> getItems()
    {
        return items;
    }

    public void setItems(ArrayList<ContratacionesItems> items)
    {
        this.items = items;
    }

    public String getDateSigned()
    {
        return dateSigned;
    }

    public void setDateSigned(String dateSigned)
    {
        this.dateSigned = dateSigned;
    }

    public ArrayList<ContratacionesDocuments> getDocuments()
    {
        return documents;
    }

    public void setDocuments(ArrayList<ContratacionesDocuments> documents)
    {
        this.documents = documents;
    }

    public ContratacionesImplementation getImplementation()
    {
        return implementation;
    }

    public void setImplementation(ContratacionesImplementation implementation)
    {
        this.implementation = implementation;
    }

    public ArrayList<ContratacionesRelatedProcesses> getRelatedProcesses()
    {
        return relatedProcesses;
    }

    public void setRelatedProcesses(ArrayList<ContratacionesRelatedProcesses> relatedProcesses)
    {
        this.relatedProcesses = relatedProcesses;
    }

    public ArrayList<ContratacionesMilestones> getMilestones()
    {
        return milestones;
    }

    public void setMilestones(ArrayList<ContratacionesMilestones> milestones)
    {
        this.milestones = milestones;
    }

    public ArrayList<ContratacionesAmendments> getAmendments()
    {
        return amendments;
    }

    public void setAmendments(ArrayList<ContratacionesAmendments> amendments)
    {
        this.amendments = amendments;
    }

    public ContratacionesAmendments getAmendment()
    {
        return amendment;
    }

    public void setAmendment(ContratacionesAmendments amendment)
    {
        this.amendment = amendment;
    }

}