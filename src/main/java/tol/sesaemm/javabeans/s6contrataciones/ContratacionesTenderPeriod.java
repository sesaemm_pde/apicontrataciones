/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesTenderPeriod implements Serializable
{

    private String startDate;
    private String endDate;
    private String maxExtentDate;
    private Integer durationInDays;

    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public String getMaxExtentDate()
    {
        return maxExtentDate;
    }

    public void setMaxExtentDate(String maxExtentDate)
    {
        this.maxExtentDate = maxExtentDate;
    }

    public Integer getDurationInDays()
    {
        return durationInDays;
    }

    public void setDurationInDays(Integer durationInDays)
    {
        this.durationInDays = durationInDays;
    }

}
