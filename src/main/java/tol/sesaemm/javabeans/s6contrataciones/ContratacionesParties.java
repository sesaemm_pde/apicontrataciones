/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaaemm.org.mx
 */
public class ContratacionesParties implements Serializable
{

    private String name;
    @BsonProperty("id")
    private String id;
    private ContratacionesIdentifier identifier;
    private ArrayList<ContratacionesIdentifier> additionalIdentifiers;
    private ContratacionesAddress address;
    private ContratacionesContactPoint contactPoint;
    private ArrayList<String> roles;
    private ContratacionesDetails details;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public ContratacionesIdentifier getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(ContratacionesIdentifier identifier)
    {
        this.identifier = identifier;
    }

    public ArrayList<ContratacionesIdentifier> getAdditionalIdentifiers()
    {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(ArrayList<ContratacionesIdentifier> additionalIdentifiers)
    {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public ContratacionesAddress getAddress()
    {
        return address;
    }

    public void setAddress(ContratacionesAddress address)
    {
        this.address = address;
    }

    public ContratacionesContactPoint getContactPoint()
    {
        return contactPoint;
    }

    public void setContactPoint(ContratacionesContactPoint contactPoint)
    {
        this.contactPoint = contactPoint;
    }

    public ArrayList<String> getRoles()
    {
        return roles;
    }

    public void setRoles(ArrayList<String> roles)
    {
        this.roles = roles;
    }

    public ContratacionesDetails getDetails()
    {
        return details;
    }

    public void setDetails(ContratacionesDetails details)
    {
        this.details = details;
    }

}
