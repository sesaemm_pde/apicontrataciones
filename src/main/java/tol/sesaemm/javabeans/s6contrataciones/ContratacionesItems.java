/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesItems implements Serializable
{

    private Object id;
    private String description;
    private ContratacionesClassification classification;
    private ArrayList<ContratacionesClassification> additionalClassifications;
    private Integer quantity;
    private ContratacionesUnit unit;

    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public ContratacionesClassification getClassification()
    {
        return classification;
    }

    public void setClassification(ContratacionesClassification classification)
    {
        this.classification = classification;
    }

    public ArrayList<ContratacionesClassification> getAdditionalClassifications()
    {
        return additionalClassifications;
    }

    public void setAdditionalClassifications(ArrayList<ContratacionesClassification> additionalClassifications)
    {
        this.additionalClassifications = additionalClassifications;
    }

    public Integer getQuantity()
    {
        return quantity;
    }

    public void setQuantity(Integer quantity)
    {
        this.quantity = quantity;
    }

    public ContratacionesUnit getUnit()
    {
        return unit;
    }

    public void setUnit(ContratacionesUnit unit)
    {
        this.unit = unit;
    }

}
