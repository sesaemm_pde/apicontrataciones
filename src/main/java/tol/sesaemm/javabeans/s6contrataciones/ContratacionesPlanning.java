/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesPlanning implements Serializable
{

    private String rationale;
    private ContratacionesBudget budget;
    private ArrayList<ContratacionesDocuments> documents;
    private ArrayList<ContratacionesMilestones> milestones;

    public String getRationale()
    {
        return rationale;
    }

    public void setRationale(String rationale)
    {
        this.rationale = rationale;
    }

    public ContratacionesBudget getBudget()
    {
        return budget;
    }

    public void setBudget(ContratacionesBudget budget)
    {
        this.budget = budget;
    }

    public ArrayList<ContratacionesDocuments> getDocuments()
    {
        return documents;
    }

    public void setDocuments(ArrayList<ContratacionesDocuments> documents)
    {
        this.documents = documents;
    }

    public ArrayList<ContratacionesMilestones> getMilestones()
    {
        return milestones;
    }

    public void setMilestones(ArrayList<ContratacionesMilestones> milestones)
    {
        this.milestones = milestones;
    }

}
