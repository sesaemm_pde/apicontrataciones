/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesAwards implements Serializable
{

    private Object id;
    private String title;
    private String description;
    private String status;
    private String date;
    private ContratacionesAmount value;
    private ArrayList<ContratacionesTenderers> suppliers;
    private ArrayList<ContratacionesItems> items;
    private ContratacionesTenderPeriod contractPeriod;
    private ArrayList<ContratacionesDocuments> documents;
    private ArrayList<ContratacionesAmendments> amendments;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private ContratacionesAmendments amendment;
    //</editor-fold>
    
    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public ContratacionesAmount getValue()
    {
        return value;
    }

    public void setValue(ContratacionesAmount value)
    {
        this.value = value;
    }

    public ArrayList<ContratacionesTenderers> getSuppliers()
    {
        return suppliers;
    }

    public void setSuppliers(ArrayList<ContratacionesTenderers> suppliers)
    {
        this.suppliers = suppliers;
    }

    public ArrayList<ContratacionesItems> getItems()
    {
        return items;
    }

    public void setItems(ArrayList<ContratacionesItems> items)
    {
        this.items = items;
    }

    public ContratacionesTenderPeriod getContractPeriod()
    {
        return contractPeriod;
    }

    public void setContractPeriod(ContratacionesTenderPeriod contractPeriod)
    {
        this.contractPeriod = contractPeriod;
    }

    public ArrayList<ContratacionesDocuments> getDocuments()
    {
        return documents;
    }

    public void setDocuments(ArrayList<ContratacionesDocuments> documents)
    {
        this.documents = documents;
    }

    public ArrayList<ContratacionesAmendments> getAmendments()
    {
        return amendments;
    }

    public void setAmendments(ArrayList<ContratacionesAmendments> amendments)
    {
        this.amendments = amendments;
    }

    public ContratacionesAmendments getAmendment()
    {
        return amendment;
    }

    public void setAmendment(ContratacionesAmendments amendment)
    {
        this.amendment = amendment;
    }

}