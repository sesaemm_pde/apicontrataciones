/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesEstadoSeccion
{
 
    private Integer numero_seccion;
    private String nombre_seccion;
    private String estado_seccion;
    private String fecha_registro;
    private String fecha_modificacion;
    private String usuario_registro;
    private String usuario_modificacion;
    private ArrayList<ContratacionesEstadoBloque> bloques_seccion;

    public Integer getNumero_seccion()
    {
        return numero_seccion;
    }

    public void setNumero_seccion(Integer numero_seccion)
    {
        this.numero_seccion = numero_seccion;
    }

    public String getNombre_seccion()
    {
        return nombre_seccion;
    }

    public void setNombre_seccion(String nombre_seccion)
    {
        this.nombre_seccion = nombre_seccion;
    }

    public String getEstado_seccion()
    {
        return estado_seccion;
    }

    public void setEstado_seccion(String estado_seccion)
    {
        this.estado_seccion = estado_seccion;
    }

    public String getFecha_registro()
    {
        return fecha_registro;
    }

    public void setFecha_registro(String fecha_registro)
    {
        this.fecha_registro = fecha_registro;
    }

    public String getFecha_modificacion()
    {
        return fecha_modificacion;
    }

    public void setFecha_modificacion(String fecha_modificacion)
    {
        this.fecha_modificacion = fecha_modificacion;
    }

    public String getUsuario_registro()
    {
        return usuario_registro;
    }

    public void setUsuario_registro(String usuario_registro)
    {
        this.usuario_registro = usuario_registro;
    }

    public String getUsuario_modificacion()
    {
        return usuario_modificacion;
    }

    public void setUsuario_modificacion(String usuario_modificacion)
    {
        this.usuario_modificacion = usuario_modificacion;
    }

    public ArrayList<ContratacionesEstadoBloque> getBloques_seccion()
    {
        return bloques_seccion;
    }

    public void setBloques_seccion(ArrayList<ContratacionesEstadoBloque> bloques_seccion)
    {
        this.bloques_seccion = bloques_seccion;
    }

}