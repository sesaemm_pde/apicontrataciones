/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesEstadoBloque
{
    
    private Integer numero_bloque;
    private String nombre_bloque;
    private String estado_bloque;

    public Integer getNumero_bloque()
    {
        return numero_bloque;
    }

    public void setNumero_bloque(Integer numero_bloque)
    {
        this.numero_bloque = numero_bloque;
    }

    public String getNombre_bloque()
    {
        return nombre_bloque;
    }

    public void setNombre_bloque(String nombre_bloque)
    {
        this.nombre_bloque = nombre_bloque;
    }

    public String getEstado_bloque()
    {
        return estado_bloque;
    }

    public void setEstado_bloque(String estado_bloque)
    {
        this.estado_bloque = estado_bloque;
    }

}