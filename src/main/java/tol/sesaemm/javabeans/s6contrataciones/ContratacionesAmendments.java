/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesAmendments implements Serializable
{

    private String date;
    private String rationale;
    @BsonProperty("id")
    private String id;
    private String description;
    private String amendsReleaseID;
    private String releaseID;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private ArrayList<ContratacionesChanges> changes;
    //</editor-fold>
    
    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getRationale()
    {
        return rationale;
    }

    public void setRationale(String rationale)
    {
        this.rationale = rationale;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAmendsReleaseID()
    {
        return amendsReleaseID;
    }

    public void setAmendsReleaseID(String amendsReleaseID)
    {
        this.amendsReleaseID = amendsReleaseID;
    }

    public String getReleaseID()
    {
        return releaseID;
    }

    public void setReleaseID(String releaseID)
    {
        this.releaseID = releaseID;
    }

    public ArrayList<ContratacionesChanges> getChanges()
    {
        return changes;
    }

    public void setChanges(ArrayList<ContratacionesChanges> changes)
    {
        this.changes = changes;
    }

}