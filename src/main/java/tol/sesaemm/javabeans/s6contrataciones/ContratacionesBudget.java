/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesBudget implements Serializable
{

    private Object id;
    private String description;
    private ContratacionesAmount amount;
    private String project;
    private Object projectID;
    private String uri;
    //<editor-fold defaultstate="collapsed" desc="deprecated">
    private String source;
    //</editor-fold>
    
    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public ContratacionesAmount getAmount()
    {
        return amount;
    }

    public void setAmount(ContratacionesAmount amount)
    {
        this.amount = amount;
    }

    public String getProject()
    {
        return project;
    }

    public void setProject(String project)
    {
        this.project = project;
    }

    public Object getProjectID()
    {
        return projectID;
    }

    public void setProjectID(Object projectID)
    {
        this.projectID = projectID;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

}