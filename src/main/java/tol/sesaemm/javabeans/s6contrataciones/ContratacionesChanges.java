/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesChanges
{
    
    private String property;
    private Object former_value;

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public Object getFormer_value()
    {
        return former_value;
    }

    public void setFormer_value(Object former_value)
    {
        this.former_value = former_value;
    }

}