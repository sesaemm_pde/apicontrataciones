/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import java.util.ArrayList;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesRelatedProcesses implements Serializable
{

    @BsonProperty("id")
    private String id;
    private ArrayList<String> relationship;
    private String title;
    private String scheme;
    private String identifier;
    private String uri;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public ArrayList<String> getRelationship()
    {
        return relationship;
    }

    public void setRelationship(ArrayList<String> relationship)
    {
        this.relationship = relationship;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getScheme()
    {
        return scheme;
    }

    public void setScheme(String scheme)
    {
        this.scheme = scheme;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

}
