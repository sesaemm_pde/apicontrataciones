/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s6contrataciones;

import java.io.Serializable;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 *
 * @author Ismael Ortz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesUnit implements Serializable
{

    private String scheme;
    @BsonProperty("id")
    private String id;
    private String name;
    private ContratacionesAmount value;
    private String uri;

    public String getScheme()
    {
        return scheme;
    }

    public void setScheme(String scheme)
    {
        this.scheme = scheme;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public ContratacionesAmount getValue()
    {
        return value;
    }

    public void setValue(ContratacionesAmount value)
    {
        this.value = value;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

}
